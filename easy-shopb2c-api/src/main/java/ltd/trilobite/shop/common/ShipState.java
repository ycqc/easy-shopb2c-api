package ltd.trilobite.shop.common;

public interface ShipState {
    /**
     * 待发货
     */
    int SEND_NO = 1;
    /**
     * 发货中
     */
    int SENDING = 2;
    /**
     * 已发货
     */
    int SEND_YES = 3;
}
