package ltd.trilobite.shop.common;

public interface OrderState {
    /**
     * 待付款
     */
    int PAYMENT_NO = 1;
    /**
     * 已付款
     */
    int PAYMENT_YES = 2;
    /**
     * 已完成
     */
    int COMPLETE = 3;
}
