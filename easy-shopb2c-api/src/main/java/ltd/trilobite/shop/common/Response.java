package ltd.trilobite.shop.common;

import ltd.trilobite.sdk.status.Result;

public class Response {
    public static final Result SUCCESS = new Result(200, 1, true);
    public static final Result FAIL = new Result(-1, -1, true);
}
