package ltd.trilobite.shop.common;

public enum ProductType {
    PRIVILEGE(1, "权益包商品"),
    GOODS(2, "实体商品");

    private long value;
    private String name;

    private ProductType(long value, String name){
        this.value = value;
        this.name = name;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
