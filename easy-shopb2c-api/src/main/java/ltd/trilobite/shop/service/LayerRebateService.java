package ltd.trilobite.shop.service;

import ltd.trilobite.shop.dao.*;
import ltd.trilobite.shop.dao.entry.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class LayerRebateService {
    Logger logs = LogManager.getLogger(LayerRebateService.class);
    GiveGoodsConfigDao giveGoodsConfigDao = new GiveGoodsConfigDao();
    BillDao billDao = new BillDao();

    MemberStatusDao memberStatusDao = new MemberStatusDao();
    GlobalSetDao globalSetDao = new GlobalSetDao();


    /**
     * 订单审核完成后执行
     * 管理奖返利
     *
     * @return 200成功
     */
    private void execute(MemberOrder order, List<Object> list) {

        GlobalSet globalsetParam = new GlobalSet();
        globalsetParam.setGlobalSetId(2);
        GlobalSet globleset = globalSetDao.findOne(globalsetParam, GlobalSet.class);

        PersonLayer param = new PersonLayer();
        param.setMemberId(order.getPersonId());
        param.setDepth(Integer.parseInt(globleset.getConstVal()));

        List<PersonLayer> layers = memberStatusDao.layerParent(param);
        GiveGoodsConfig giveGoodsConfig = new GiveGoodsConfig();
        giveGoodsConfig.setProductId(order.getMemberProductId());
        giveGoodsConfig.setCode("3");
        List<GiveGoodsConfig> givegoods = giveGoodsConfigDao.list(giveGoodsConfig, GiveGoodsConfig.class);
        billDao.delActionSrcId(order.getOrderId(), "2");
        for (PersonLayer layer : layers) {
            for (GiveGoodsConfig givegood : givegoods) {
                Bill bill = new Bill();
                bill.setNum(givegood.getNum());
                bill.setCode(givegood.getGoodsCode());
                bill.setPersonId(layer.getParentId());
                bill.setState(1);
                bill.setSrcId(order.getOrderId());
                bill.setActionCode("2");
                bill.setDescription("管理提成:"+ layer.getDepth() + "L");
                list.add(bill);
            }

        }

    }
}
