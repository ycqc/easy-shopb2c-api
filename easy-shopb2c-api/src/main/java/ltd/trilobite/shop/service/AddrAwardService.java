package ltd.trilobite.shop.service;

import ltd.trilobite.shop.dao.*;
import ltd.trilobite.shop.dao.entry.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class AddrAwardService {
    Logger logs = LogManager.getLogger(AddrAwardService.class);
    GiveGoodsConfigDao giveGoodsConfigDao = new GiveGoodsConfigDao();
    BillDao billDao = new BillDao();
    AreaDao areaDao=new AreaDao();
    CityDao cityDao=new CityDao();
    ProvinceDao provinceDao=new ProvinceDao();

    MemberStatusDao memberStatusDao = new MemberStatusDao();
    GlobalSetDao globalSetDao = new GlobalSetDao();
    PersonCertificationDao personCertificationDao=new PersonCertificationDao();
    PersonAddrAwardConfigDao personAddrAwardConfigDao=new PersonAddrAwardConfigDao();
    AddrBillDao addrBillDao=new AddrBillDao();
    /**
     * 订单审核完成后执行
     * 管理奖返利
     *
     * @return 200成功
     */
    public void execute(MemberOrder order, List<Object> list) {
        addrBillDao.delForOrderId(order.getOrderId());
       Long personId= order.getPersonId();
       PersonCertification pc= personCertificationDao.findById(personId);
       if(pc.getIsAudit()==1){
           logs.info("当前用户已经实名认证");
           int i=0;
           GlobalSet param1=new GlobalSet();
           param1.setGlobalSetId(14);
           Double price=Double.parseDouble(globalSetDao.findOne(param1,GlobalSet.class).getConstVal());
           if(pc.getArea()!=null){
               i++;
               logs.info("执行区县");
               Area param=new Area();
               param.setCode(pc.getArea());
               Area area= areaDao.findOne(param,Area.class);

               Long id= personAddrAwardConfigDao.add(area.getCode(),area.getName());
               AddrBill addrBill=new AddrBill();
               addrBill.setNum(price);

               addrBill.setPersonAddrAwardConfigId(id);
               addrBill.setOrderId(order.getOrderId());
               list.add(addrBill);

           }
           if(pc.getCity()!=null){
               i++;
               logs.info("执行城市");
               City param=new City();
               param.setCode(pc.getCity());
               City city= cityDao.findOne(param,City.class);
               Long id= personAddrAwardConfigDao.add(city.getCode(),city.getName());
               AddrBill addrBill=new AddrBill();
               addrBill.setNum(price);
               addrBill.setPersonAddrAwardConfigId(id);
               addrBill.setOrderId(order.getOrderId());
               list.add(addrBill);
           }
           if(pc.getProvince()!=null){

              if(i<2){
                  logs.info("执行省级");
                  Province param=new Province();
                  param.setCode(pc.getProvince());
                  Province province= provinceDao.findOne(param,Province.class);
                  Long id= personAddrAwardConfigDao.add(province.getCode(),province.getName());
                  AddrBill addrBill=new AddrBill();
                  addrBill.setNum(price);
                  addrBill.setPersonAddrAwardConfigId(id);
                  addrBill.setOrderId(order.getOrderId());
                  list.add(addrBill);
              }
           }


       }

    }
}
