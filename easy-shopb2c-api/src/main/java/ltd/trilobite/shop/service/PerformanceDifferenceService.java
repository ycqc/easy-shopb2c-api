package ltd.trilobite.shop.service;

import ltd.trilobite.shop.dao.*;
import ltd.trilobite.shop.dao.entry.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class PerformanceDifferenceService {
    Logger logs = LogManager.getLogger(PerformanceDifferenceService.class);

    BillDao billDao = new BillDao();

    MemberStatusDao memberStatusDao = new MemberStatusDao();
    GlobalSetDao globalSetDao = new GlobalSetDao();

    PersonAchievementsDao personAchievementsDao = new PersonAchievementsDao();
    PerformanceDifferConfigDao performanceDifferConfigDao = new PerformanceDifferConfigDao();

    /**
     * 绩差奖励
     */
    public void execute(AchievementsSettlement achievementsSettlement) {
        logs.info("开始计算绩差奖励");
        logs.info("正在通过全局设置中获取全局配置");
        GlobalSet globalsetParam = new GlobalSet();
        globalsetParam.setGlobalSetId(1);
        GlobalSet globleset = globalSetDao.findOne(globalsetParam, GlobalSet.class);
        logs.info("读取绩差配置完成，每单固定按照" + globleset.getConstVal());
        Long pv = Long.parseLong(globleset.getConstVal());
        List<PersonAchievements> personAchievements = personAchievementsDao.findNewAdd(achievementsSettlement.getStartTime(), achievementsSettlement.getEndTime());

        logs.info("读取" + new Date(achievementsSettlement.getStartTime().getTime()) + "到" + new Date(achievementsSettlement.getStartTime().getTime()) + "的新增业绩");

        //极差配置
        PerformanceDifferConfig pdconfigParam = new PerformanceDifferConfig();
        List<PerformanceDifferConfig> performanceDifferConfis = performanceDifferConfigDao.list(pdconfigParam, PerformanceDifferConfig.class);

        String newaddPerson = "";
        int i = 0;
        Map<Long, List<Long>> orderPerson = new HashMap<>();
        Map<Long, Long> orderBuied = new HashMap<>();
        for (PersonAchievements personAddNew : personAchievements) {
            if (!orderPerson.containsKey(personAddNew.getOrderId())) {
                orderPerson.put(personAddNew.getOrderId(), new ArrayList<>());
            }
            if (!orderBuied.containsKey(personAddNew.getOrderId())) {
                orderBuied.put(personAddNew.getOrderId(), personAddNew.getOrderPersonId());
            }
            orderPerson.get(personAddNew.getOrderId()).add(personAddNew.getPersonId());
            if (i > 0) {
                newaddPerson += ",";
            }
            newaddPerson += personAddNew.getPersonId().toString();
            i++;
        }

        Map<Long, Long> team = personAchievementsDao.findTem(achievementsSettlement.getEndTime(), newaddPerson);
        logs.info("团队业绩情况" + team);
        logs.info("参与新业绩分配的所有人" + newaddPerson);
        List<Object> list = new ArrayList<>();
        for (Map.Entry en : orderBuied.entrySet()) {
            logs.info("订单:" + en.getKey());
            logs.info("购买人:" + en.getValue());
            billDao.delActionSrcId((Long) en.getKey(), "1");
            PersonLayer param = new PersonLayer();
            param.setMemberId((Long) en.getValue());
            List<PersonLayer> layers = memberStatusDao.layerParentNoDepth(param);
            int savep = 0;
            for (PersonLayer layer : layers) {
                logs.info("深度" + layer.getDepth());
                logs.info("人员" + layer.getParentId());
                logs.info("团队业绩" + team.get(layer.getParentId()));
                PerformanceDifferConfig conf = checkConfig(team.get(layer.getParentId()), performanceDifferConfis);

                if (conf != null) {
                    logs.info(conf.getLevelNum());
                    logs.info(conf.getAchievement());
                    if (conf.getProportion() > savep) {


                        Bill bill = new Bill();

                        int lastp = savep;
                        savep = conf.getProportion() - savep;
                        bill.setNum(pv * savep / 100d);
                        bill.setCode("5");
                        bill.setPersonId(layer.getParentId());
                        bill.setState(1);
                        bill.setSrcId((Long) en.getKey());
                        bill.setActionCode("1");
                        if (lastp == 0) {
                            bill.setDescription("[业绩奖励]无业绩分出,你的业绩比例为：" + conf.getProportion() + "%，实际比例为：" + savep + "%");

                        } else {
                            bill.setDescription("[业绩奖励]已有业绩：" + lastp + "%已分出,你的业绩比例为：" + conf.getProportion() + "%，实际比例为：" + savep + "%");
                        }
                        list.add(bill);

                    } else {
                        logs.info("平级，无法拿到业绩");
                    }

                    logs.info("可拿到比例" + conf.getProportion() + ":" + savep);
                } else {
                    logs.info("未达到要求，无法拿到业绩");
                }
            }

        }

        billDao.adds(list);

//       for(PersonAchievements personAddNew:personAchievements) {
//           PerformanceDifferConfig conf = checkConfig(team.get(personAddNew.getPersonId()), performanceDifferConfis);
//           if (conf != null) {
//               logs.info("新增业绩来自订单"+personAddNew.getOrderId());
//               logs.info("查看当前人员"+personAddNew.getPersonId()+"新增业绩"+personAddNew.getNum());
//               logs.info(conf.getLevelNum());
//               logs.info(conf.getAchievement());
//               logs.info(conf.getProportion());
//               logs.info("检查下级人员是否和我平级");
//               logs.info("参与人编号"+orderPerson.get(personAddNew.getOrderId()));
//
//           }else{
//               logs.info("未达到级差要求，无法拿到业绩");
//           }
//       }


        //找到新增业绩的团队业绩，以及配置中所属的会员级别

        //找到业绩的人员组织结构关系

        //查找是否下面成员是否给你平级，如果平级就不拿
        //如果未平级，那么查看


    }

    private PerformanceDifferConfig checkConfig(Long num, List<PerformanceDifferConfig> performanceDifferConfis) {
        PerformanceDifferConfig def = null;
        Long min = 0l;
        for (PerformanceDifferConfig performanceDifferConfig : performanceDifferConfis) {

            if (num > performanceDifferConfig.getAchievement()) {
                def = performanceDifferConfig;
                if (performanceDifferConfig.getAchievement() > min) {
                    def = performanceDifferConfig;
                    min = performanceDifferConfig.getAchievement();
                }
            }

        }
        return def;

    }
}
