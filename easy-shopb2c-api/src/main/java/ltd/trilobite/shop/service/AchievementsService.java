package ltd.trilobite.shop.service;

import ltd.trilobite.shop.dao.*;
import ltd.trilobite.shop.dao.entry.MemberOrder;
import ltd.trilobite.shop.dao.entry.MemberProduct;
import ltd.trilobite.shop.dao.entry.PersonAchievements;
import ltd.trilobite.shop.dao.entry.PersonLayer;

import java.util.List;

/**
 * 业绩计算服务
 */

public class AchievementsService {

    MemberStatusDao memberStatusDao = new MemberStatusDao();

    MemberProductDao memberProductDao = new MemberProductDao();

    PersonAchievementsDao personAchievementsDao = new PersonAchievementsDao();

    /**
     * 计算业绩
     */
    public void execute(MemberOrder order, List<Object> list) {
        MemberProduct memberProductParam = new MemberProduct();
        memberProductParam.setMemberProductId(order.getMemberProductId());
        MemberProduct memberProduct = memberProductDao.findOne(memberProductParam, MemberProduct.class);
        PersonLayer param = new PersonLayer();
        param.setMemberId(order.getPersonId());
        personAchievementsDao.delForSrcId(order.getOrderId());
        List<PersonLayer> layers = memberStatusDao.layerParentNoDepth(param);

        for (PersonLayer layer : layers) {
           if(layer.getParentId()!=null) {
               PersonAchievements personAchievements = new PersonAchievements();
               personAchievements.setOrderId(order.getOrderId());
               personAchievements.setOrderPersonId(order.getPersonId());
               personAchievements.setPersonId(layer.getParentId());
               personAchievements.setCreateTime(order.getCreateTime());
               personAchievements.setNum(memberProduct.getPrice());
               list.add(personAchievements);
           }
        }
    }
}
