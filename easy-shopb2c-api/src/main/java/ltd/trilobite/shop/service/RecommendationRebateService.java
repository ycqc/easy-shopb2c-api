package ltd.trilobite.shop.service;

import ltd.trilobite.shop.dao.BillDao;
import ltd.trilobite.shop.dao.GiveGoodsConfigDao;

import ltd.trilobite.shop.dao.MemberStatusDao;
import ltd.trilobite.shop.dao.entry.Bill;
import ltd.trilobite.shop.dao.entry.GiveGoodsConfig;
import ltd.trilobite.shop.dao.entry.MemberOrder;
import ltd.trilobite.shop.dao.entry.MemberStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * 推荐返利服务
 */
public class RecommendationRebateService {

    Logger logs = LogManager.getLogger(LayerRebateService.class);
    GiveGoodsConfigDao giveGoodsConfigDao = new GiveGoodsConfigDao();
    BillDao billDao = new BillDao();

    MemberStatusDao memberStatusDao = new MemberStatusDao();

    /**
     * 订单审核完成后执行
     * 推荐返利
     *
     * @return 200成功
     */
    public  void execute(MemberOrder order, List<Object> list) {
        logs.info("推荐返利开始");

        GiveGoodsConfig giveGoodsConfig = new GiveGoodsConfig();
        giveGoodsConfig.setProductId(order.getMemberProductId());
        giveGoodsConfig.setCode("2");//设置推荐返利
        List<GiveGoodsConfig> givegoods = giveGoodsConfigDao.list(giveGoodsConfig, GiveGoodsConfig.class);
        MemberStatus msparam = new MemberStatus();
        msparam.setMemberId(order.getPersonId());
        logs.info("查找父亲人员编号");
        MemberStatus parentMemer = memberStatusDao.findOne(msparam, MemberStatus.class);
        billDao.delActionSrcId(order.getOrderId(), "4");

        for (GiveGoodsConfig givegood : givegoods) {
            Bill bill = new Bill();
            bill.setNum(givegood.getNum());
            bill.setCode(givegood.getGoodsCode());
            bill.setPersonId(parentMemer.getParentId());
            bill.setState(1);
            bill.setSrcId(order.getOrderId());
            bill.setDescription("推荐获取礼包");
            bill.setActionCode("4");
            list.add(bill);
        }

    }
}
