package ltd.trilobite.shop.service;

import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.OrderState;
import ltd.trilobite.shop.common.ProductType;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.common.ShipState;
import ltd.trilobite.shop.dao.*;
import ltd.trilobite.shop.dao.entry.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * 订单服务
 */
public class OrderService {
    private static final int BALANCE_DEDUCT_RATE_GLOBAL_SET_ID = 9;
    private static final int LOG_STATUS_PAYMENT_YES = 1;
    private static final int LOG_STATUS_SEND_NO = 2;
    private static final int LOG_STATUS_SENDING = 3;
    private static final int LOG_STATUS_SEND_YES = 4;
    private static final int LOG_STATUS_COMPLETE = 5;

    //复效
    private static final int RE_CONSUMPTION_YES = 2;
    //非复效
    private static final int RE_CONSUMPTION_NO = 1;

    private static final int COMPLETE_STATE = 3;

    private static final String BALANCE_CODE = "6";
    private static final String ORDER_BALANCE_DEDUCT_ACTION_CODE = "11";




    Logger logs = LogManager.getLogger(OrderService.class);

    BillDao billDao = new BillDao();
    MemberOrderDao memberOrderDao = new MemberOrderDao();
    GlobalSetDao globalSetDao = new GlobalSetDao();
    MemberProductDao memberProductDao = new MemberProductDao();
    MemberOrderLogDao memberOrderLogDao = new MemberOrderLogDao();
    ProductReExtDao productReExtDao = new ProductReExtDao();
    ProductExtDao productExtDao = new ProductExtDao();
    MemberStatusDao memberStatusDao = new MemberStatusDao();
    PersonAddrDao personAddrDao = new PersonAddrDao();
    RedPackagesService redPackagesService=new RedPackagesService();
    ValidRecommendService validRecommendService=new ValidRecommendService();
    AchievementsService achievementsService=new AchievementsService();
    RechargeRebateService rechargeRebateService=new RechargeRebateService();
    RecommendationRebateService recommendationRebateService=new RecommendationRebateService();
    AddrAwardService addrAwardService=new AddrAwardService();
    public Result add(MemberOrder memberOrder, MemberProduct memberProduct) {

        Result result = new Result(200);
        Double price;
        String productTitle;
        //获取已支付非复消订单数量
        long count = memberOrderDao.getValidOrderCount(memberOrder.getPersonId());
        if(count>0){
            result.setCode(-5);
            return result;
        }

        ProductExt productExt = productExtDao.findById(memberOrder.getMemberProductId());
        price = memberProduct.getPrice();
        productTitle = productExt.getTitle();
        memberOrder.setIsReConsumption(RE_CONSUMPTION_NO);




        Map<String, String> receiverInfo = personAddrDao.getReceiverInfo(memberOrder.getPersonAddrId());

        Long orderId = memberOrderDao.getSeq("member_order");
        memberOrder.setOrderId(orderId);
        memberOrder.setPayPrice(price);
        memberOrder.setProductTitle(productTitle);
        memberOrder.setReceiverAddr(receiverInfo.get("receiverAddr"));
        memberOrder.setReceiverPerson(receiverInfo.get("receiverPerson"));
        memberOrder.setReceiverPhonenum(receiverInfo.get("receiverPhonenum"));
        memberOrderDao.add(memberOrder);


        result.setData(memberOrder.getOrderId());
        return result;
    }



    /**
     * 设置已付款
     * （在这里结算收益）
     * @param orderId
     * @param personId
     * @return
     */
    public Result setOrderStateToPaymentYes(Long orderId, Long personId) {
        MemberOrder memberOrder = memberOrderDao.findById(orderId);
        if (memberOrder != null && memberOrder.getOrderState() == OrderState.PAYMENT_NO) {
            memberOrderDao.updateOrderState(memberOrder.getOrderId(), OrderState.PAYMENT_YES);
            memberOrderDao.updateShipState(memberOrder.getOrderId(), ShipState.SEND_NO);
            memberOrderLogDao.add(orderId, personId, LOG_STATUS_PAYMENT_YES);
            memberOrderLogDao.add(orderId, personId, LOG_STATUS_SEND_NO);

            MemberProduct memberProduct = memberProductDao.findById(memberOrder.getMemberProductId());
            //冗余有效用户
            MemberStatus param=new MemberStatus();
            param.setMemberId(memberOrder.getPersonId());
            MemberStatus memberStatus= memberStatusDao.findOne(param,MemberStatus.class);
            if(memberStatus!=null) {
               memberStatusDao.updateValidMember(memberStatus.getParentId());
            }
            List<Object> saveObjects=new ArrayList<>();
            Double rewardPrice = getRewardPrice(memberOrder)*memberOrder.getNum();
            //true 可购买的产品 false 权益产品
            if (memberProduct.getProductTypeId() == ProductType.GOODS.getValue()) {
                //Double rewardPrice = getRewardPrice(memberOrder)*memberOrder.getNum();

                //返报单红包及将红包转化为循环钱包
                redPackagesService.execute(memberOrder,saveObjects,rewardPrice);
                //推荐奖
                validRecommendService.execute(memberOrder,saveObjects,rewardPrice);
                //计算业绩

                //addRedPacketBill(memberOrder, rewardPrice);
                //执行荐点奖
                //recommendRewardService.rebateRecommendReward(memberOrder, rewardPrice);
            } else if (memberProduct.getProductTypeId() == ProductType.PRIVILEGE.getValue()) {
                //timelyRebate(memberOrder);
                rechargeRebateService.execute(memberOrder,saveObjects);
                recommendationRebateService.execute(memberOrder,saveObjects);
                validRecommendService.execute(memberOrder,saveObjects,rewardPrice);
                addrAwardService.execute(memberOrder,saveObjects);
            }

            achievementsService.execute(memberOrder,saveObjects);

            billDao.adds(saveObjects);

            return Response.SUCCESS;
        } else {
            return Response.FAIL;
        }
    }




    /**
     * 获取用于奖励的金额
     * @param memberOrder
     * @return
     */
    private Double getRewardPrice(MemberOrder memberOrder) {
        //奖励的金额
        Double rewardPrice;
        if(memberOrder.getIsReConsumption()==2){
            rewardPrice = productReExtDao.getRewardPrice(memberOrder.getMemberProductId());
        }else{
            rewardPrice = productExtDao.getRewardPrice(memberOrder.getMemberProductId());
        }
        return rewardPrice;
    }

    /**
     * 设置发货中
     *
     * @param orderId
     * @param personId
     * @return
     */
    public Result setOrderStateToSending(Long orderId, Long personId) {
        MemberOrder memberOrder = memberOrderDao.findById(orderId);
        if (memberOrder != null
                && memberOrder.getOrderState() == OrderState.PAYMENT_YES
                && memberOrder.getIsSend() == ShipState.SEND_NO) {
            memberOrderDao.updateShipState(memberOrder.getOrderId(), ShipState.SENDING);
            memberOrderLogDao.add(orderId, personId, LOG_STATUS_SENDING);
            return Response.SUCCESS;
        } else {
            return Response.FAIL;
        }
    }

    /**
     * 设置已发货
     *
     * @param orderId
     * @param personId
     * @return
     */
    public Result setOrderStateToSendYes(Long orderId, Long personId) {
        MemberOrder memberOrder = memberOrderDao.findById(orderId);
        if (memberOrder != null
                && memberOrder.getOrderState() == OrderState.PAYMENT_YES
                && (memberOrder.getIsSend() == ShipState.SEND_NO || memberOrder.getIsSend() == ShipState.SENDING)) {
            memberOrderDao.updateShipState(memberOrder.getOrderId(), ShipState.SEND_YES);
            memberOrderLogDao.add(orderId, personId, LOG_STATUS_SEND_YES);
            return Response.SUCCESS;
        } else {
            return Response.FAIL;
        }
    }

    /**
     * 设置已完成
     *
     * @param orderId
     * @param personId
     * @return
     */
    public Result setOrderStateToComplete(Long orderId, Long personId) {
        MemberOrder memberOrder = memberOrderDao.findById(orderId);
        if (memberOrder != null
                && memberOrder.getOrderState() == OrderState.PAYMENT_YES
                && memberOrder.getIsSend() == ShipState.SEND_YES) {
            memberOrderDao.updateOrderState(memberOrder.getOrderId(), OrderState.COMPLETE);
            memberOrderLogDao.add(orderId, personId, LOG_STATUS_COMPLETE);
            billDao.updateStateComplet(orderId);
            return Response.SUCCESS;
        }else{
            return Response.FAIL;
        }
    }


    public void timelyRebate(MemberOrder order) {
        //当订单审核通过后执行
        if (order.getOrderState() == 3) {
            List<Object> list = new ArrayList<>();
            //rechargeRebate(order, list);
           // recommendationRebate(order, list);
           // LayerRebate(order, list);
           // achievements(order, list);
            billDao.adds(list);
        }
    }





}
