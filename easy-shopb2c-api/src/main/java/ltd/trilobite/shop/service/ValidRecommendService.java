package ltd.trilobite.shop.service;

import com.alibaba.fastjson.JSON;
import ltd.trilobite.shop.dao.MemberStatusDao;
import ltd.trilobite.shop.dao.RecommendReawardPersonConfigDao;
import ltd.trilobite.shop.dao.RecommendRewardConfigDao;
import ltd.trilobite.shop.dao.entry.Bill;
import ltd.trilobite.shop.dao.entry.MemberOrder;
import ltd.trilobite.shop.dao.entry.MemberStatus;
import ltd.trilobite.shop.dao.entry.PersonLayer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 有效推荐服务
 */
public class ValidRecommendService {
    Logger logs = LogManager.getLogger(OrderService.class);
    MemberStatusDao memberStatusDao = new MemberStatusDao();
    RecommendRewardConfigDao recommendRewardConfigDao = new RecommendRewardConfigDao();
    RecommendReawardPersonConfigDao recommendReawardPersonConfigDao = new RecommendReawardPersonConfigDao();
    /**
     * 计算有效推荐账单
     * @param billList
     */
    public void execute(MemberOrder order, List<Object> billList, double rewadPrice){
        logs.info("开始计算有效推荐服务");
        DecimalFormat df = new DecimalFormat("#.00");
        df.setMaximumFractionDigits(2);
        df.setGroupingSize(0);
        df.setRoundingMode(RoundingMode.FLOOR);
        Map<Integer, Integer> recommendReawardPersonConfig = recommendReawardPersonConfigDao.getConfigs();

        //层数和返利比例配置
        Map<Integer, Double> recommendReawardConfig = recommendRewardConfigDao.getConfigs();
        int maxLayer=recommendRewardConfigDao.getMaxRebateLayer();
        int maxPerson=recommendReawardPersonConfigDao.getMaxPersonNum();
        MemberStatus param=new MemberStatus();
        param.setMemberId(order.getPersonId());
        MemberStatus memberStatus= memberStatusDao.findOne(param,MemberStatus.class);
        if(memberStatus==null){
            logs.info("上级为空，计算终止");
            return;
        }
        PersonLayer p=new PersonLayer();
        p.setMemberId(memberStatus.getParentId());


        List<PersonLayer> list=memberStatusDao.layerParentNoDepth(p);
        Iterator<PersonLayer> iterator= list.iterator();
        int currlayer=0;
        while (iterator.hasNext()){
            PersonLayer personLayer=  iterator.next();
            logs.info("用户编号："+personLayer.getMemberId()+"："+personLayer.getParentId());

            Bill bill=new Bill();

            bill.setSrcId(order.getOrderId());
            bill.setCreateTime(new Date());
            bill.setActionCode("4");
            bill.setCode("5");
            if(personLayer.getDepth()==1){
                currlayer++;

                double conPro=recommendReawardConfig.get(1);
                bill.setNum(rewadPrice*conPro/100d);
                bill.setPersonId(personLayer.getMemberId());
                bill.setDescription(order.getOrderId()+"-1L-"+conPro);
                logs.info("当前处于第一层的用户拿直推将，比例为{},奖金可拿{}",conPro,df.format(rewadPrice*conPro/100d));
                billList.add(bill);

            }else{

                if(currlayer>=maxLayer){
                    logs.info("有效层超出最大层，不能获取奖励");
                    break;
                }
                if(personLayer.getNextPersonNum()==null||personLayer.getNextPersonNum()==0){
                    logs.info("当前推荐用户为０,不能那见点奖");
                  continue;
                }
                if(personLayer.getNextPersonNum()>=maxPerson){
                    personLayer.setNextPersonNum(maxPerson);
                }

                int canLayer= recommendReawardPersonConfig.get(personLayer.getNextPersonNum());
                if(canLayer<currlayer){
                    logs.info("可拿层数小于有效层数，不能拿去奖励{}",personLayer.getNextPersonNum());
                    continue;
                }
                //if(personLayer.getNextPersonNum()>=3){
                    currlayer++;
                    double conPro=recommendReawardConfig.get(currlayer);
                    bill.setNum(rewadPrice*conPro/100d);
                    bill.setPersonId(personLayer.getMemberId());
                    bill.setState(3);
                    bill.setDescription(order.getOrderId()+"-"+currlayer+"L-"+personLayer.getNextPersonNum()+"P-"+conPro);
                    billList.add(bill);
                    logs.info("当前处于{}的用户{},比例为{},有效深度为{},奖金可拿{}",personLayer.getDepth(),personLayer.getMemberId(),conPro,currlayer,df.format(rewadPrice*conPro/100d));
               // }else{
                    logs.info("当前处于"+personLayer.getDepth()+"的用户:"+personLayer.getMemberId()+"不能拿直推");
               // }


            }
            if(personLayer.getParentId()==null){
                break;
            }
        }
        //logs.info(JSON.toJSONString(recommendReawardPersonConfig));
        //logs.info(JSON.toJSONString(recommendReawardConfig));
        //logs.info(JSON.toJSONString(list));

    }
}
