package ltd.trilobite.shop.service;

import ltd.trilobite.shop.dao.BillDao;
import ltd.trilobite.shop.dao.GiveGoodsConfigDao;
import ltd.trilobite.shop.dao.entry.Bill;
import ltd.trilobite.shop.dao.entry.GiveGoodsConfig;
import ltd.trilobite.shop.dao.entry.MemberOrder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * 充值返利
 */
public class RechargeRebateService {

    Logger logs = LogManager.getLogger(RechargeRebateService.class);
    GiveGoodsConfigDao giveGoodsConfigDao = new GiveGoodsConfigDao();
    BillDao billDao = new BillDao();

    /**
     * 订单审核完成后执行
     * 充值返利
     *
     * @return 200成功
     */
    public void execute(MemberOrder order, List<Object> list) {
        logs.info("充值返利");
        GiveGoodsConfig giveGoodsConfig = new GiveGoodsConfig();
        giveGoodsConfig.setProductId(order.getMemberProductId());
        giveGoodsConfig.setCode("1");//设置充值返利
        List<GiveGoodsConfig> givegoods = giveGoodsConfigDao.list(giveGoodsConfig, GiveGoodsConfig.class);
        billDao.delActionSrcId(order.getOrderId(), "3");
        for (GiveGoodsConfig givegood : givegoods) {
            Bill bill = new Bill();
            bill.setNum(givegood.getNum());
            bill.setCode(givegood.getGoodsCode());
            bill.setPersonId(order.getPersonId());
            bill.setState(1);
            bill.setSrcId(order.getOrderId());
            bill.setActionCode("3");
            bill.setDescription("充值礼包");
            list.add(bill);
        }

    }

}
