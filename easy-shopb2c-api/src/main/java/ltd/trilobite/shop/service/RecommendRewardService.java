package ltd.trilobite.shop.service;

import ltd.trilobite.shop.dao.*;
import ltd.trilobite.shop.dao.entry.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 荐点奖
 */
public class RecommendRewardService {
    private static final Logger LOGGER = LogManager.getLogger(RecommendRewardService.class);

    /**
     * 荐点奖动作代码
     */
    private static final String RECOMMEND_REWARD_ACTION_CODE = "9";

    /**
     * 积分代码
     */
    private static final String SCORE_CODE = "5";

    /**
     * 已完成
     */
    private static final int COMPLETE_STATE = 3;

    BillDao billDao = new BillDao();
    PersonDao personDao = new PersonDao();
    MemberProductDao memberProductDao = new MemberProductDao();
    ProductReExtDao productReExtDao = new ProductReExtDao();
    MemberStatusDao memberStatusDao =  new MemberStatusDao();
    PersonAchievementsDao personAchievementsDao = new PersonAchievementsDao();
    RecommendRewardConfigDao recommendRewardConfigDao = new RecommendRewardConfigDao();
    RecommendReawardPersonConfigDao recommendReawardPersonConfigDao = new RecommendReawardPersonConfigDao();

    /**
     * 荐点奖返利
     * @param memberOrder 订单
     */
    public void rebateRecommendReward(MemberOrder memberOrder, Double rewardPrice) {
        LOGGER.info("执行荐点奖返利");

        //查询上级会员
        Person parent = personDao.findParent(memberOrder.getPersonId());
        if(parent == null){
            return;
        }
        LOGGER.info("上级会员personId:{}", parent.getPersonId());

        //当前产品价格
        Double productPrice = getProductPrice(memberOrder);

        //新增上级业绩
        PersonAchievements personAchievements = new PersonAchievements();
        personAchievements.setPersonId(parent.getPersonId());
        personAchievements.setOrderPersonId(memberOrder.getPersonId());
        personAchievements.setOrderId(memberOrder.getOrderId());
        personAchievements.setNum(productPrice);
        personAchievements.setCreateTime(new Date());
        personAchievementsDao.add(personAchievements);

        LOGGER.info("当前产品价格:{}", productPrice);
        LOGGER.info("新增上级业绩:{}", productPrice);
        LOGGER.info("用于奖励的金额:{}", rewardPrice);

        if (rewardPrice != null && rewardPrice > 0) {
            //荐点奖最大返利层数
            Integer maxRebateLayer = recommendRewardConfigDao.getMaxRebateLayer();
            LOGGER.info("荐点奖最大返利层数maxRebateLayer:{}", maxRebateLayer);
            PersonLayer param = new PersonLayer();
            param.setMemberId(memberOrder.getPersonId());
            param.setDepth(maxRebateLayer);

            List<PersonLayer> layers = memberStatusDao.layerParent(param);
            if(layers.isEmpty()){
                return;
            }

            //推荐人数和能拿层数配置
            Map<Integer, Integer> recommendReawardPersonConfig = recommendReawardPersonConfigDao.getConfigs();
            //层数和返利比例配置
            Map<Integer, Double> recommendReawardConfig = recommendRewardConfigDao.getConfigs();
            //荐点奖最大推荐人数
            Integer maxPersonNum = recommendReawardPersonConfigDao.getMaxPersonNum();
            LOGGER.info("荐点奖最大推荐人数maxPersonNum:{}", maxPersonNum);

            //报单红包返现比率
            List<String> idList = layers.stream().map(layer -> String.valueOf(layer.getParentId())).collect(Collectors.toList());
            Map<Long, Integer> recommendPersonCountMap = memberStatusDao.getRecommendPersonCountMap(idList);

            if(recommendPersonCountMap.isEmpty()){
                return;
            }

            layers.forEach(layer -> {
                //直推人数
                Integer recommendPersonCount = recommendPersonCountMap.get(layer.getParentId());
                if(isRebate(layer, recommendPersonCount, maxPersonNum, recommendReawardPersonConfig)){
                    doRebate(layer, recommendPersonCount, memberOrder, rewardPrice, recommendReawardConfig);
                }
            });
        }
    }

    /**
     * 执行荐点奖返利
     * @param layer 当前会员层级
     * @param memberOrder 订单
     * @param rewardPrice 用于奖励的金额
     * @param recommendReawardConfig 层数和返利比例配置
     */
    private void doRebate(PersonLayer layer, Integer recommendPersonCount, MemberOrder memberOrder, Double rewardPrice, Map<Integer, Double> recommendReawardConfig) {
        Double proportion = recommendReawardConfig.get(layer.getDepth());
        Double rebateScore = (rewardPrice * proportion) / 100d;

        Bill bill = new Bill();
        bill.setPersonId(layer.getParentId());
        bill.setNum(rebateScore);
        bill.setSrcId(memberOrder.getOrderId());
        bill.setCode(SCORE_CODE);
        bill.setActionCode(RECOMMEND_REWARD_ACTION_CODE);
        bill.setState(COMPLETE_STATE);
        bill.setDescription("来自于"+layer.getDepth()+"L-"+recommendPersonCount+"P");
        bill.setCreateTime(new Date());
        billDao.add(bill);

        LOGGER.info("荐点奖返利personId:"+layer.getParentId()+"-"+bill.getDescription());
    }

    /**
     * 判断是否能拿荐点奖
     * @param personLayer 当前会员层级
     * @param maxPersonNum 配置中最大推荐人数
     * @param recommendReawardPersonConfig 推荐人数和能拿层数配置
     * @return
     */
    private boolean isRebate(PersonLayer personLayer, Integer recommendPersonCount, Integer maxPersonNum, Map<Integer, Integer> recommendReawardPersonConfig) {

        if(recommendPersonCount == 0){
            return false;
        }
        //通过直推人数取得能拿荐点奖层数
        if(recommendPersonCount > maxPersonNum){
            recommendPersonCount = maxPersonNum;
        }
        Integer rebateLayers = recommendReawardPersonConfig.get(recommendPersonCount);
        //当前层数大于能拿荐点奖层数
        if(personLayer.getDepth() > rebateLayers){
            return false;
        }
        return true;
    }

    /**
     * 获取产品价格
     * @param memberOrder
     * @return
     */
    private Double getProductPrice(MemberOrder memberOrder) {
        //奖励的金额
        Double productPrice;
        if(memberOrder.getIsReConsumption() == 2){
            productPrice = productReExtDao.getPrice(memberOrder.getMemberProductId());
        }else {
            productPrice = memberProductDao.getPrice(memberOrder.getMemberProductId());
        }
        return productPrice;
    }

}
