package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.OrderState;
import ltd.trilobite.shop.common.ProductType;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.dao.*;
import ltd.trilobite.shop.dao.entry.MemberOrder;
import ltd.trilobite.shop.dao.entry.MemberOrderLog;
import ltd.trilobite.shop.dao.entry.MemberProduct;
import ltd.trilobite.shop.dao.entry.Person;
import ltd.trilobite.shop.service.OrderService;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

@ApiDesc(desc = "3002-会员订单")
@RestService
public class MemberOrderAction {
    OrderService orderService = new OrderService();
    PersonDao personDao = new PersonDao();
    MemberOrderDao memberOrderDao = new MemberOrderDao();
    MemberProductDao memberProductDao = new MemberProductDao();
    MemberOrderLogDao memberOrderLogDao = new MemberOrderLogDao();
    PersonAddrDao personAddrDao = new PersonAddrDao();

    @ApiDoc(title = "新增权益商品订单", param = {
            @ApiDesc(name = "memberProductId", type = "header", desc = "产品编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-参数错误 -2:会员类型错误"),
            }

    )
    @Router(path = "/memberOrder/addPrivilegeOrder")
    @Proxy(target = AuthProxy.class)
    public Result addPrivilegeOrder(RestForm form) {
        MemberProduct memberProduct = memberProductDao.findById(Long.parseLong(form.get("memberProductId")));
        if(memberProduct.getProductTypeId() != ProductType.PRIVILEGE.getValue()){
            return new Result(-1);
        }

        Person person = personDao.findById(Long.parseLong(form.getHeader().get("personId")));
        if (person.getMemberType() == null) {
            return new Result(-2);
        }

        Long orderId = memberOrderDao.getSeq("member_order");

        MemberOrder memberOrder = new MemberOrder();
        memberOrder.setOrderId(orderId);
        memberOrder.setMemberProductId(memberProduct.getMemberProductId());
        memberOrder.setPersonId(person.getPersonId());
        memberOrder.setPayPrice(memberProduct.getPrice());
        memberOrder.setProductTitle(memberProduct.getName());
        memberOrder.setNum(1);
        memberOrder.setOrderState(OrderState.PAYMENT_NO);
        memberOrder.setCreateTime(new Date());
        memberOrderDao.add(memberOrder);
        Result result = new Result(200);
        result.setData(memberOrder.getOrderId());
        return result;
    }

    @ApiDoc(title = "新增普通产品订单", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "personAddrId", type = "string", desc = "地址ID"),
            @ApiDesc(name = "integralPrice", type = "string", desc = "积分抵扣金额"),
            @ApiDesc(name = "num", type = "string", desc = "产品数量"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string",
                            desc = "响应码 200-成功 -1-参数错误 -2:余额不足 -3:余额抵扣不能超过订单价格的50% -4-失败，存在付款订单"),
            }
    )
    @Router(path = "/memberOrder/addGoodsOrder")
    @Proxy(target = AuthProxy.class)
    public Result addGoodsOrder(RestForm form) {
        //积分
        Double integralPrice;
        if(StringUtils.isBlank(form.get("integralPrice"))){
           integralPrice = 0d;
        }else{
           integralPrice = Double.parseDouble(form.get("integralPrice"));
        }

        //产品数量
        Integer num = Integer.parseInt(form.get("num"));

        MemberProduct memberProduct = memberProductDao.findById(Long.parseLong(form.get("memberProductId")));
//        if(memberProduct.getProductTypeId() != ProductType.GOODS.getValue()
//                || integralPrice < 0
//                || num < 0){
//            return new Result(-1);
//        }

        Long personId = Long.parseLong(form.getHeader().get("personId"));

//        Long payNoOrderCount =  memberOrderDao.getPayNoOrderCount(personId);
//
//        if(payNoOrderCount > 0){ //失败，存在付款订单
//            return new Result(-4);
//        }

        MemberOrder memberOrder = new MemberOrder();
        memberOrder.setMemberProductId(memberProduct.getMemberProductId());
        memberOrder.setPersonId(personId);
        memberOrder.setPersonAddrId(Long.parseLong(form.get("personAddrId")));
        memberOrder.setIntegralPrice(integralPrice);
        memberOrder.setNum(num);
        memberOrder.setOrderState(OrderState.PAYMENT_NO);
        memberOrder.setCreateTime(new Date());

        return orderService.add(memberOrder, memberProduct);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/memberOrder/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form) {
        Long orderId = Long.parseLong(form.get("orderId"));
        MemberOrderLog memberOrderLog = new MemberOrderLog();
        memberOrderLog.setOrderId(orderId);
        memberOrderLogDao.del(memberOrderLog);

        MemberOrder order = new MemberOrder();
        order.setOrderId(orderId);
        memberOrderDao.del(order);
        return Response.SUCCESS;
    }

    @ApiDoc(title = "查询会员有效订单数量", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/memberOrder/findValidOrderCount")
    @Proxy(target = AuthProxy.class)
    public Result findValidOrderCount(RestForm form) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        long count = memberOrderDao.getValidOrderCount(personId);
        Result result = new Result(200);
        result.setData(count);
        return result;
    }

    @ApiDoc(title = "修改凭证", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
            @ApiDesc(name = "payCredential", type = "string", desc = "支付凭据"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/memberOrder/updateCredential")
    @Proxy(target = AuthProxy.class)
    public Result updateCredential(RestForm form) {
        MemberOrder order = new MemberOrder();
        order.setOrderId(Long.parseLong(form.get("orderId")));
        order.setPayCredential(form.get("payCredential"));
        memberOrderDao.update(order);
        return Response.SUCCESS;
    }


    @ApiDoc(title = "标记状态为已付款", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/memberOrder/OrderStateToPaymentYes")
    @Proxy(target = AuthProxy.class)
    public Result setOrderStateToPaymentYes(RestForm form) {
        Long orderId = Long.parseLong(form.get("orderId"));
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return orderService.setOrderStateToPaymentYes(orderId, personId);
    }

    @ApiDoc(title = "标记状态为发货中", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/memberOrder/OrderStateToSending")
    @Proxy(target = AuthProxy.class)
    public Result setOrderStateToSending(RestForm form) {
        Long orderId = Long.parseLong(form.get("orderId"));
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return orderService.setOrderStateToSending(orderId, personId);
    }

    @ApiDoc(title = "标记状态为已发货", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/memberOrder/OrderStateToSendYes")
    @Proxy(target = AuthProxy.class)
    public Result setOrderStateToSendYes(RestForm form) {
        Long orderId = Long.parseLong(form.get("orderId"));
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return orderService.setOrderStateToSendYes(orderId, personId);
    }

    @ApiDoc(title = "标记状态为完成", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/memberOrder/OrderStateToComplete")
    @Proxy(target = AuthProxy.class)
    public Result setOrderStateToComplete(RestForm form) {
        Long orderId = Long.parseLong(form.get("orderId"));
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return orderService.setOrderStateToComplete(orderId, personId);
    }

    @ApiDoc(title = "查询详细信息", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "personAddrId", type = "string", desc = "地址ID"),
                    @ApiDesc(name = "payCredential", type = "string", desc = "支付凭证"),
                    @ApiDesc(name = "orderState", type = "string", desc = "订单状态 1-待付款 2-已付款 3-已完成"),
                    @ApiDesc(name = "isSend", type = "string", desc = "发货状态 1-待发货 2-发货中 3-已发货"),
                    @ApiDesc(name = "productTitle", type = "string", desc = "产品名称"),
                    @ApiDesc(name = "num", type = "string", desc = "产品数量"),
                    @ApiDesc(name = "payPrice", type = "string", desc = "支付价格"),
                    @ApiDesc(name = "integralPrice", type = "string", desc = "积分抵扣价格"),
                    @ApiDesc(name = "logisticsName", type = "string", desc = "物流公司"),
                    @ApiDesc(name = "logisticsNum", type = "string", desc = "物流单号"),
                    @ApiDesc(name = "logisticsPrice", type = "string", desc = "物流价格"),
                    @ApiDesc(name = "receiverAddr", type = "string", desc = "收货人地址"),
                    @ApiDesc(name = "receiverPerson", type = "string", desc = "收货人"),
                    @ApiDesc(name = "receiverPhonenum", type = "string", desc = "收货人手机号"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间")
            }
    )
    @Router(path = "/memberOrder/detail")
    @Proxy(target = AuthProxy.class)
    public Result detail(RestForm form) {
        return new Result(memberOrderDao.findDetail(Long.parseLong(form.get("orderId"))));
    }

    @ApiDoc(title = "我的订单统计", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "orderState1", type = "string", desc = "待付款数量"),
                    @ApiDesc(name = "orderState2", type = "string", desc = "已付款数量"),
                    @ApiDesc(name = "orderState3", type = "string", desc = "已完成"),
                    @ApiDesc(name = "isSend1", type = "string", desc = "待发货"),
                    @ApiDesc(name = "isSend2", type = "string", desc = "发货中"),
                    @ApiDesc(name = "isSend3", type = "string", desc = "已发货"),
            }
    )
    @Router(path = "/memberOrder/myOrderCount")
    @Proxy(target = AuthProxy.class)
    public Result myOrderCount(RestForm form) {
        return new Result(memberOrderDao.findMyOrderCount(Long.parseLong(form.getHeader().get("personId"))));
    }

    @ApiDoc(title = "我的订单－分页查询", param = {
            @ApiDesc(name = "orderState", type = "string", desc = "订单状态 1-待付款 2-已付款 3-已完成"),
            @ApiDesc(name = "isSend", type = "string", desc = "发货状态 1-待发货 2-发货中 3-已发货"),
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "personAddrId", type = "string", desc = "地址ID"),
                    @ApiDesc(name = "payCredential", type = "string", desc = "支付凭证"),
                    @ApiDesc(name = "orderState", type = "string", desc = "订单状态 1-待付款 2-已付款 3-已完成"),
                    @ApiDesc(name = "isSend", type = "string", desc = "发货状态 1-待发货 2-发货中 3-已发货"),
                    @ApiDesc(name = "productTitle", type = "string", desc = "产品名称"),
                    @ApiDesc(name = "num", type = "string", desc = "产品数量"),
                    @ApiDesc(name = "payPrice", type = "string", desc = "支付价格"),
                    @ApiDesc(name = "integralPrice", type = "string", desc = "积分抵扣价格"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),

            }
    )
    @Router(path = "/memberOrder/myOrders", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result myOrders(RestForm form) {
        return memberOrderDao.findMyOrders(form);
    }


    @ApiDoc(title = "分页查询", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "编号"),
            @ApiDesc(name = "orderState", type = "string", desc = "订单状态 1-待付款 2-已付款 3-已完成"),
            @ApiDesc(name = "isSend", type = "string", desc = "发货状态 1-待发货 2-发货中 3-已发货"),
            @ApiDesc(name = "personName", type = "string", desc = "姓名"),
            @ApiDesc(name = "phoneNum", type = "string", desc = "手机号"),
            @ApiDesc(name = "gender", type = "string", desc = "性别"),
            @ApiDesc(name = "receiverPerson", type = "string", desc = "收货人"),
            @ApiDesc(name = "receiverPhonenum", type = "string", desc = "收货人手机号"),
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "personId", type = "string", desc = "用户ID"),
                    @ApiDesc(name = "personName", type = "string", desc = "姓名"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "手机号"),
                    @ApiDesc(name = "gender", type = "string", desc = "性别"),
                    @ApiDesc(name = "personAddrId", type = "string", desc = "地址ID"),
                    @ApiDesc(name = "payCredential", type = "string", desc = "支付凭证"),
                    @ApiDesc(name = "orderState", type = "string", desc = "订单状态 1-待付款 2-已付款 3-已完成"),
                    @ApiDesc(name = "isSend", type = "string", desc = "发货状态 1-待发货 2-发货中 3-已发货"),
                    @ApiDesc(name = "productTitle", type = "string", desc = "产品名称"),
                    @ApiDesc(name = "num", type = "string", desc = "数量"),
                    @ApiDesc(name = "payPrice", type = "string", desc = "支付价格"),
                    @ApiDesc(name = "integralPrice", type = "string", desc = "积分抵扣价格"),
                    @ApiDesc(name = "logisticsName", type = "string", desc = "物流公司"),
                    @ApiDesc(name = "logisticsNum", type = "string", desc = "物流单号"),
                    @ApiDesc(name = "logisticsPrice", type = "string", desc = "物流价格"),
                    @ApiDesc(name = "receiverAddr", type = "string", desc = "收货人地址"),
                    @ApiDesc(name = "receiverPerson", type = "string", desc = "收货人"),
                    @ApiDesc(name = "receiverPhonenum", type = "string", desc = "收货人手机号"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),

            }
    )
    @Router(path = "/memberOrder/NaviList", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findNaviList(RestForm form) {
        return memberOrderDao.findNaviList(form);
    }


}
