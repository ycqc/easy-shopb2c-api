package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.common.dao.PersonDao;
import ltd.trilobite.common.entry.Person;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.dao.BillDao;
import ltd.trilobite.shop.dao.GlobalSetDao;
import ltd.trilobite.shop.dao.entry.GlobalSet;

@ApiDesc(desc = "3004-账单")
@RestService
public class BillAction {
    BillDao billDao = new BillDao();
    PersonDao personDao=new PersonDao();
    GlobalSetDao globalSetDao=new GlobalSetDao();
    @ApiDoc(title = "查询剩余转换余额",param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "剩余抵扣余额")
            }
    )
    @Router(path = "/bill/YcJfBalance")
    @Proxy(target = AuthProxy.class)
    public Result findDeductBalance(RestForm form){
        return new Result(billDao.findYcJfBalance(Long.parseLong(form.getHeader().get("personId"))));
    }



    @ApiDoc(title = "账单转换",param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
            @ApiDesc(name = "num", type = "string", desc = "转换数量"),
            @ApiDesc(name = "targetCode", type = "string", desc = "转换目标"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败　-2 转换最低不能小于 -3 余额不足"),

            }
    )
    @Router(path = "/bill/convert")
    @Proxy(target = AuthProxy.class)
    public Result convert(RestForm form,@Param Person person){
        GlobalSet param=new GlobalSet();
        param.setGlobalSetId(11);
        GlobalSet globalSet=globalSetDao.findOne(param,GlobalSet.class);

        GlobalSet param1=new GlobalSet();
        param1.setGlobalSetId(12);
        GlobalSet GlobalSet1=globalSetDao.findOne(param1,GlobalSet.class);
        int plamPrice=Integer.parseInt(GlobalSet1.getConstVal()) ;

        Long personId=Long.parseLong(form.getHeader().get("personId"));


        if(Double.parseDouble(form.get("num"))<Double.parseDouble(globalSet.getConstVal())){
            return new Result(-2,globalSet.getConstVal());
        }
        if(Double.parseDouble(form.get("num"))>billDao.findYcJfBalance(personId)){
            return new Result(-3);
        }
        if (!personDao.hasOldPayPass(person.getPayPass(), personId)) {
            return new Result(-4);//支付密码不正确
        }

        return new Result(billDao.convert(personId,form,plamPrice));
    }


}
