package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;

import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.dao.PersonAddrDao;
import ltd.trilobite.shop.dao.entry.PersonAddr;

@ApiDesc(desc = "4003-人员地址")
@RestService
public class PersonAddrAction {
    PersonAddrDao personAddrDao=new PersonAddrDao();

    @ApiDoc(title = "新增",param = {
            @ApiDesc(name = "personAddrId", type = "string", desc = "内容编号"),
            @ApiDesc(name = "addrDetail", type = "string", desc = "内容名称"),
            @ApiDesc(name = "revPersonName", type = "string", desc = "内容名称"),
            @ApiDesc(name = "phoneNum", type = "string", desc = "收件人电话"),
            @ApiDesc(name = "province", type = "string", desc = "省"),
            @ApiDesc(name = "city", type = "string", desc = "市"),
            @ApiDesc(name = "area", type = "string", desc = "区县"),
            @ApiDesc(name = "street", type = "string", desc = "街道"),
            @ApiDesc(name = "token",type = "header",desc = "密匙")
            },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }

    )
    @Router(path = "/personAddr/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param PersonAddr personAddr){
        Long personId= Long.parseLong(form.getHeader().get("personId"));
        personAddr.setIsDefault(0);
        personAddr.setPersonId(personId);
        personAddrDao.add(personAddr);
        return Response.SUCCESS;
    }

    @ApiDoc(title = "设置默认地址",param = {
            @ApiDesc(name = "personAddrId", type = "string", desc = "地址ID"),
            @ApiDesc(name = "token",type = "header",desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/personAddr/default")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form){
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        Long personAddrId = Long.parseLong(form.get("personAddrId"));
        personAddrDao.setDefault(personId, personAddrId);
        return Response.SUCCESS;
    }

    @ApiDoc(title = "修改",param = {
            @ApiDesc(name = "personAddrId", type = "string", desc = "内容编号"),
            @ApiDesc(name = "addrDetail", type = "string", desc = "内容名称"),
            @ApiDesc(name = "revPersonName", type = "string", desc = "内容名称"),
            @ApiDesc(name = "phoneNum", type = "string", desc = "收件人电话"),
            @ApiDesc(name = "province", type = "string", desc = "省"),
            @ApiDesc(name = "city", type = "string", desc = "市"),
            @ApiDesc(name = "area", type = "string", desc = "区县"),
            @ApiDesc(name = "street", type = "string", desc = "街道"),
            @ApiDesc(name = "token",type = "header",desc = "密匙")
            },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/personAddr/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form,  @Param PersonAddr personAddr){
        personAddrDao.update(personAddr);
        return Response.SUCCESS;
    }
    @ApiDoc(title = "删除",param = {
            @ApiDesc(name = "personAddrId",type = "string",desc = "内容编号"),
            @ApiDesc(name = "token",type = "header",desc = "密匙"),
            },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/personAddr/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form,  @Param PersonAddr personAddr){
        personAddrDao.del(personAddr);
        return Response.SUCCESS;
    }

    @ApiDoc(title = "查询详细信息",param = {
            @ApiDesc(name = "personAddrId",type = "string",desc = "内容编号"),
            @ApiDesc(name = "token",type = "header",desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "personAddrId", type = "string", desc = "内容编号"),
                    @ApiDesc(name = "addrDetail", type = "string", desc = "内容名称"),
                    @ApiDesc(name = "revPersonName", type = "string", desc = "内容名称"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "收件人电话"),
                    @ApiDesc(name = "province", type = "string", desc = "省"),
                    @ApiDesc(name = "city", type = "string", desc = "市"),
                    @ApiDesc(name = "area", type = "string", desc = "区县"),
                    @ApiDesc(name = "street", type = "string", desc = "街道"),
                    @ApiDesc(name = "isDefault", type = "string", desc = "默认地址"),
            }
    )
    @Router(path = "/personAddr/Detail")
    @Proxy(target = AuthProxy.class)
    public Result detail(RestForm form){
        Long personAddrId = Long.parseLong(form.get("personAddrId"));
        return new Result(personAddrDao.findDetail(personAddrId));
    }

    @ApiDoc(title = "查询默认地址",param = {
            @ApiDesc(name = "token",type = "header",desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "personAddrId", type = "string", desc = "内容编号"),
                    @ApiDesc(name = "addrDetail", type = "string", desc = "内容名称"),
                    @ApiDesc(name = "revPersonName", type = "string", desc = "内容名称"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "收件人电话"),
                    @ApiDesc(name = "province", type = "string", desc = "省"),
                    @ApiDesc(name = "city", type = "string", desc = "市"),
                    @ApiDesc(name = "area", type = "string", desc = "区县"),
                    @ApiDesc(name = "street", type = "string", desc = "街道"),
                    @ApiDesc(name = "isDefault", type = "string", desc = "默认地址"),
            }
    )
    @Router(path = "/personAddr/findDefault")
    @Proxy(target = AuthProxy.class)
    public Result findDefault(RestForm form){
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return new Result(personAddrDao.findDefault(personId));
    }

    @ApiDoc(title = "单表查询",param = {

            @ApiDesc(name = "token",type = "header",desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "personAddrId", type = "string", desc = "内容编号"),
                    @ApiDesc(name = "addrDetail", type = "string", desc = "内容名称"),
                    @ApiDesc(name = "revPersonName", type = "string", desc = "内容名称"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "收件人电话"),
                    @ApiDesc(name = "province", type = "string", desc = "省"),
                    @ApiDesc(name = "city", type = "string", desc = "市"),
                    @ApiDesc(name = "area", type = "string", desc = "区县"),
                    @ApiDesc(name = "street", type = "string", desc = "街道"),
            }
    )
    @Router(path = "/personAddr/List",method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param PersonAddr  personAddr) {
        Long personId= Long.parseLong(form.getHeader().get("personId"));
        return new Result(personAddrDao.myList(personId));
    }

}
