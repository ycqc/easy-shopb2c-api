package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.dao.GiveGoodsConfigDao;
import ltd.trilobite.shop.dao.entry.GiveGoodsConfig;

@ApiDesc(desc = "4002-礼包")
@RestService
public class GiveGoodConfigAction {

    GiveGoodsConfigDao giveGoodsConfigDao=new GiveGoodsConfigDao();
    @ApiDoc(title = "新增",param = {
            @ApiDesc(name = "giveGoodsConfigId", type = "string", desc = "编号"),

            @ApiDesc(name = "token", type = "header", desc = "密匙")
            },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }

    )
    @Router(path = "/giveGoodsConfig/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param GiveGoodsConfig giveGoodsConfig){
        giveGoodsConfigDao.add(giveGoodsConfig);
        return Response.SUCCESS;
    }

    @ApiDoc(title = "修改",param = {
            @ApiDesc(name = "giveGoodsConfigId",type = "string",desc = "编号"),

            @ApiDesc(name = "token",type = "header",desc = "密匙")
            },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/giveGoodsConfig/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form,  @Param GiveGoodsConfig giveGoodsConfig){
        giveGoodsConfigDao.update(giveGoodsConfig);
        return Response.SUCCESS;
    }
    @ApiDoc(title = "删除",param = {
            @ApiDesc(name = "giveGoodsConfigId",type = "string",desc = "编号"),
            @ApiDesc(name = "token",type = "header",desc = "密匙"),
            },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/giveGoodsConfig/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form,  @Param GiveGoodsConfig giveGoodsConfig){
        giveGoodsConfigDao.del(giveGoodsConfig);
        return Response.SUCCESS;
    }

    @ApiDoc(title = "单表查询",param = {

            @ApiDesc(name = "token",type = "header",desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "giveGoodsConfigId",type = "string",desc = "编号"),
                    @ApiDesc(name = "name",type = "string",desc = "公司名称"),
            }
    )
    @Router(path = "/giveGoodsConfig/List",method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form,  @Param GiveGoodsConfig giveGoodsConfig) {
        return new Result(giveGoodsConfigDao.list(giveGoodsConfig,GiveGoodsConfig.class));
    }
}
