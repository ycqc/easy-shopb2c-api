package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.dao.LogisticsInvoiceDao;
import ltd.trilobite.shop.dao.entry.LogisticsInvoice;

@ApiDesc(desc = "3003-订单物流")
@RestService
public class LogisticsInvoiceAction {
    LogisticsInvoiceDao logisticsInvoiceDao = new LogisticsInvoiceDao();

    @ApiDoc(title = "查询物流详细", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
                    @ApiDesc(name = "logisticsInvoiceId", type = "string", desc = "物流ID"),
                    @ApiDesc(name = "logisticsCompanyId", type = "string", desc = "物流公司ID"),
                    @ApiDesc(name = "price", type = "string", desc = "价格"),
                    @ApiDesc(name = "num", type = "string", desc = "物流号")
            }
    )
    @Router(path = "/logisticsInvoice/detail")
    @Proxy(target = AuthProxy.class)
    public Result detail(RestForm form) {
        Long orderId = Long.parseLong(form.get("orderId"));
        LogisticsInvoice logisticsInvoice = new LogisticsInvoice();
        logisticsInvoice.setOrderId(orderId);
        return new Result(logisticsInvoiceDao.findOne(logisticsInvoice, LogisticsInvoice.class));
    }

    @ApiDoc(title = "保存物流信息", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
            @ApiDesc(name = "logisticsInvoiceId", type = "string", desc = "物流ID"),
            @ApiDesc(name = "logisticsCompanyId", type = "string", desc = "物流公司ID"),
            @ApiDesc(name = "price", type = "string", desc = "价格"),
            @ApiDesc(name = "num", type = "string", desc = "物流号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/logisticsInvoice/save")
    @Proxy(target = AuthProxy.class)
    public Result save(RestForm form, @Param LogisticsInvoice logisticsInvoice) {
        if(logisticsInvoice.getLogisticsInvoiceId() == null){
            logisticsInvoiceDao.add(logisticsInvoice);
        }else{
            logisticsInvoiceDao.update(logisticsInvoice);
        }
        return Response.SUCCESS;
    }
}
