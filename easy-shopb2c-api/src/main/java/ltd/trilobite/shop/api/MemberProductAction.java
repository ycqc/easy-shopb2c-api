package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.ProductType;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.dao.BaseDataDao;
import ltd.trilobite.shop.dao.GiveGoodsConfigDao;
import ltd.trilobite.shop.dao.MemberProductDao;
import ltd.trilobite.shop.dao.ProductExtDao;
import ltd.trilobite.shop.dao.entry.*;

import java.util.HashMap;
import java.util.Map;

@ApiDesc(desc = "4001-会员产品")
@RestService
public class MemberProductAction {
    /**
     * 上架
     */
    private static final int ON = 0;

    /**
     * 下架
     */
    private static final int OFF = 1;

    MemberProductDao memberProductDao = new MemberProductDao();
    GiveGoodsConfigDao giveGoodsConfigDao = new GiveGoodsConfigDao();
    BaseDataDao baseDataDao = new BaseDataDao();
    ProductExtDao productExtDao = new ProductExtDao();
    @ApiDoc(title = "保存普通产品", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "name", type = "string", desc = "名称"),
            @ApiDesc(name = "onOff", type = "string", desc = "是否上下架 0-上架 1-下架"),
            @ApiDesc(name = "price", type = "string", desc = "价格"),
            @ApiDesc(name = "htmlHeight", type = "string", desc = "html高度"),
            //拓展信息
            @ApiDesc(name = "title", type = "string", desc = "商品标题"),
            @ApiDesc(name = "title1", type = "string", desc = "商品副标题"),
            @ApiDesc(name = "smailImage", type = "string", desc = "商品图片"),
            @ApiDesc(name = "introlUrl", type = "string", desc = "商品简介URL"),
            @ApiDesc(name = "inPrice", type = "string", desc = "进货价格"),
            @ApiDesc(name = "selfProfit", type = "string", desc = "利润"),
            @ApiDesc(name = "rewardPrice", type = "string", desc = "用于奖励的部分"),

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败"),
            }

    )
    @Router(path = "/memberProduct/save")
    @Proxy(target = AuthProxy.class)
    public Result save(RestForm form, @Param MemberProduct memberProduct, @Param ProductExt productExt) {

        memberProduct.setProductTypeId(ProductType.PRIVILEGE.getValue());

        if(memberProduct.getMemberProductId() == null){
            Long memberProductId = memberProductDao.getSeq("member_product");
            memberProduct.setMemberProductId(memberProductId);
            productExt.setMemberProductId(memberProductId);
            productExt.setTitle(memberProduct.getName());
            memberProductDao.add(memberProduct);

            productExtDao.add(productExt);
        }else{
            productExt.setMemberProductId(memberProduct.getMemberProductId());
            memberProductDao.update(memberProduct);
            productExtDao.update(productExt);
        }
        return Response.SUCCESS;
    }


    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败"),
            }
    )
    @Router(path = "/memberProduct/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form) {
        MemberProduct memberProduct = new MemberProduct();
        memberProduct.setMemberProductId(Long.parseLong(form.get("memberProductId")));
        ProductExt productExt=new ProductExt();
        productExt.setMemberProductId(memberProduct.getMemberProductId());
        productExtDao.del(productExt);
        memberProductDao.del(memberProduct);

        return Response.SUCCESS;
    }


    @ApiDoc(title = "[前台]查询权益类产品详细", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "title.name", type = "string", desc = "产品名称"),
                    @ApiDesc(name = "title.price", type = "string", desc = "产品价格"),
                    @ApiDesc(name = "givegood.goodName", type = "string", desc = "物品名称"),
                    @ApiDesc(name = "givegood.num", type = "string", desc = "数量"),
                    @ApiDesc(name = "givegood.unit", type = "string", desc = "单位"),
                    @ApiDesc(name = "options.code", type = "string", desc = "代码"),
                    @ApiDesc(name = "options.label", type = "string", desc = "奖励项"),
            }
    )
    @Router(path = "/memberProduct/detail")
    @Proxy(target = AuthProxy.class)
    public Result detail(RestForm form) {
        Long memberProductId = Long.parseLong(form.get("memberProductId"));
        MemberProduct memberProduct = new MemberProduct();
        memberProduct.setMemberProductId(memberProductId);
        Object title = memberProductDao.findOne(memberProduct, MemberProduct.class);

        GiveGoodsConfig giveGoodsConfig = new GiveGoodsConfig();
        giveGoodsConfig.setProductId(memberProduct.getMemberProductId());
        Object givegood = giveGoodsConfigDao.list(giveGoodsConfig, GiveGoodsConfig.class);

        BaseData baseData = new BaseData();
        baseData.setTypeId(2);
        Object options = baseDataDao.list(baseData, BaseData.class);

        Map<String, Object> re = new HashMap<>();
        re.put("title", title);
        re.put("options", options);
        re.put("givegood", givegood);

        return new Result(re);
    }

    @ApiDoc(title = "[前台]查询权益类产品列表", param = {
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "name", type = "string", desc = "产品名称"),
                    @ApiDesc(name = "price", type = "header", desc = "产品价格"),
            }
    )
    @Router(path = "/memberProduct/myGoodsList", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result myGoodsList(RestForm form) {
        return  memberProductDao.myGoodsList(form);
    }

    @ApiDoc(title = "我订的产品统计", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "name", type = "string", desc = "名称"),
                    @ApiDesc(name = "count", type = "string", desc = "数量"),
                    @ApiDesc(name = "price", type = "string", desc = "总价"),
            }
    )
    @Router(path = "/memberProduct/myProductCount", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result myProductCount(RestForm form) {
        return new Result(memberProductDao.myProductCount(Long.parseLong(form.getHeader().get("personId"))));
    }

    @ApiDoc(title = "查询单个", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
    },
            result = {
                    //商品信息
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "productTypeId", type = "string", desc = "类型ID"),
                    @ApiDesc(name = "name", type = "string", desc = "名称"),
                    @ApiDesc(name = "onOff", type = "string", desc = "是否上下架 0-上架 1-下架"),
                    @ApiDesc(name = "price", type = "string", desc = "价格"),
            }
    )
    @Router(path = "/memberProduct/findOne", method = Router.Action.Post)
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form) {
        MemberProduct memberProduct = new MemberProduct();
        memberProduct.setMemberProductId(Long.parseLong(form.get("memberProductId")));
        return new Result(memberProductDao.findOne(memberProduct, MemberProduct.class));
    }

    @ApiDoc(title = "列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    //商品信息
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "productTypeId", type = "string", desc = "类型ID"),
                    @ApiDesc(name = "name", type = "string", desc = "名称"),
                    @ApiDesc(name = "onOff", type = "string", desc = "是否上下架 0-上架 1-下架"),
                    @ApiDesc(name = "price", type = "string", desc = "价格"),
                    @ApiDesc(name = "htmlHeight", type = "string", desc = "html高度"),
            }
    )
    @Router(path = "/memberProduct/list", method = Router.Action.Post)
    @Proxy(target = AuthProxy.class)
    public Result list(RestForm form) {
        MemberProduct memberProduct = new MemberProduct();
        memberProduct.setProductTypeId(ProductType.PRIVILEGE.getValue());
        return new Result(memberProductDao.findList());
    }

    @ApiDoc(title = "产品上架", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败"),
            }
    )
    @Router(path = "/memberProduct/on", method = Router.Action.Post)
    @Proxy(target = AuthProxy.class)
    public Result on(RestForm form) {
        MemberProduct memberProduct = new MemberProduct();
        memberProduct.setMemberProductId(Long.parseLong(form.get("memberProductId")));
        memberProduct.setOnOff(ON);
        memberProductDao.update(memberProduct);
        return Response.SUCCESS;
    }

    @ApiDoc(title = "产品下架", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败"),
            }
    )
    @Router(path = "/memberProduct/off", method = Router.Action.Post)
    @Proxy(target = AuthProxy.class)
    public Result off(RestForm form) {
        MemberProduct memberProduct = new MemberProduct();
        memberProduct.setMemberProductId(Long.parseLong(form.get("memberProductId")));
        memberProduct.setOnOff(OFF);
        memberProductDao.update(memberProduct);
        return Response.SUCCESS;
    }
}
