package ltd.trilobite.shop.api;

import io.undertow.Undertow;
import io.undertow.server.handlers.resource.PathResourceManager;
import ltd.trilobite.http.HttpApplication;
import ltd.trilobite.sdk.config.ConfigFactory;

import java.nio.file.Paths;

import static io.undertow.Handlers.resource;


public class Application {

    public static void main(String[] args) {
        ConfigFactory.newInstance().loadLocalConfig();
        Undertow server = Undertow.builder()
                .addHttpListener(8000, "0.0.0.0")
                .setHandler(resource(new PathResourceManager(Paths.get("/opt/upload"), 1000))
                        .setDirectoryListingEnabled(true))
                .build();
        server.start();

        HttpApplication.run(Application.class);
        final boolean isActive = Thread.currentThread().isAlive();
        new Thread() {
            public void run() {
                while (isActive) {
                    try {
                        Thread.sleep(50000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Runtime.getRuntime().gc();
                }
            }
        }.start();
    }
}
