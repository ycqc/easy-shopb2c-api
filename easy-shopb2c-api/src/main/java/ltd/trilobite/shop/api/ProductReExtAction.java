package ltd.trilobite.shop.api;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.dao.ProductReExtDao;
import ltd.trilobite.shop.dao.entry.ProductReExt;

@ApiDesc(desc="5002-产品复消拓展信息")
@RestService
public class ProductReExtAction {
    ProductReExtDao productReExtDao = new ProductReExtDao();

    @ApiDoc(title = "保存复消拓展信息", param = {
            //商品信息
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),

            //拓展信息
            @ApiDesc(name = "title", type = "string", desc = "商品标题"),
            @ApiDesc(name = "title1", type = "string", desc = "商品副标题"),
            @ApiDesc(name = "inPrice", type = "string", desc = "进货价格"),
            @ApiDesc(name = "selfProfit", type = "string", desc = "利润"),
            @ApiDesc(name = "rewardPrice", type = "string", desc = "用于奖励的部分"),
            @ApiDesc(name = "price", type = "string", desc = "商品价格"),

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }

    )
    @Router(path = "/productReExt/save")
    @Proxy(target = AuthProxy.class)
    public Result save(RestForm form, @Param ProductReExt productReExt) {
        productReExtDao.save(productReExt);
        return Response.SUCCESS;
    }


    @ApiDoc(title = "查询单个", param = {
            //商品信息
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "title", type = "string", desc = "商品标题"),
                    @ApiDesc(name = "title1", type = "string", desc = "商品副标题"),
                    @ApiDesc(name = "inPrice", type = "string", desc = "进货价格"),
                    @ApiDesc(name = "selfProfit", type = "string", desc = "利润"),
                    @ApiDesc(name = "rewardPrice", type = "string", desc = "用于奖励的部分"),
                    @ApiDesc(name = "price", type = "string", desc = "商品价格"),
            }

    )
    @Router(path = "/productReExt/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form) {
        ProductReExt productReExt = new ProductReExt();
        productReExt.setMemberProductId(Long.parseLong(form.get("memberProductId")));
        return new Result(productReExtDao.findOne(productReExt, ProductReExt.class));
    }
}
