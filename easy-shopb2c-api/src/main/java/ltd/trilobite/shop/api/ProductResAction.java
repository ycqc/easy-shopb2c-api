package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.dao.ProductResDao;
import ltd.trilobite.shop.dao.entry.ProductRes;

@ApiDesc(desc = "6001-产品资源")
@RestService
public class ProductResAction {
    ProductResDao productResDao = new ProductResDao();


    @ApiDoc(title = "查询资源列表", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "productResId", type = "string", desc = "资源ID"),
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "url", type = "string", desc = "资源地址"),
                    @ApiDesc(name = "extType", type = "string", desc = "资源类型")
            }
    )
    @Router(path = "/productRes/list")
    @Proxy(target = AuthProxy.class)
    public Result list(RestForm form) {
        ProductRes productRes = new ProductRes();
        productRes.setMemberProductId(Long.parseLong(form.get("memberProductId")));
        return new Result(productResDao.list(productRes, ProductRes.class));
    }

    @ApiDoc(title = "添加资源", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "url", type = "string", desc = "资源地址"),
            @ApiDesc(name = "extType", type = "string", desc = "资源类型")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败"),
            }
    )
    @Router(path = "/productRes/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param ProductRes productRes) {
        productResDao.add(productRes);
        return Response.SUCCESS;
    }

    @ApiDoc(title = "删除资源", param = {
            @ApiDesc(name = "productResId", type = "string", desc = "资源ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/productRes/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form) {
        ProductRes productRes = new ProductRes();
        productRes.setProductResId(Long.parseLong(form.get("productResId")));
        productResDao.del(productRes);
        return Response.SUCCESS;
    }
}
