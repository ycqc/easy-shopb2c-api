package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.dao.ProductTypeDao;
import ltd.trilobite.shop.dao.entry.ProductType;

import java.util.List;

@ApiDesc(desc = "4003-产品类型")
@RestService
public class ProductTypeAction {
    private ProductTypeDao productTypeDao = new ProductTypeDao();

    @ApiDoc(title = "保存产品类型",param = {
            @ApiDesc(name = "productTypeId", type = "string", desc = "产品类型ID"),
            @ApiDesc(name = "name", type = "string", desc = "类型名称"),
            @ApiDesc(name = "token",type = "header",desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败"),
            }

    )
    @Router(path = "/productType/Save")
    @Proxy(target = AuthProxy.class)
    public Result save(RestForm form, @Param ProductType productType){
        if(productType.getProductTypeId() == null){
            productTypeDao.add(productType);
        }else {
            productTypeDao.update(productType);
        }
        return Response.SUCCESS;
    }

//    @ApiDoc(title = "删除产品类型",param = {
//            @ApiDesc(name = "productTypeId", type = "string", desc = "产品类型ID"),
//            @ApiDesc(name = "token",type = "header",desc = "密匙")
//    },
//            result = {
//                    @ApiDesc(name = "true", type = "string", desc = "成功"),
//            }
//
//    )
//    @Router(path = "/productType/Delete")
//    @Proxy(target = AuthProxy.class)
//    public Result delete(RestForm form){
//        Long id = Long.parseLong(form.get("productTypeId"));
//        ProductType productType = new ProductType();
//        productType.setProductTypeId(id);
//        productTypeDao.del(productType);
//        return new Result(Constants.SUCCESS);
//    }

    @ApiDoc(title = "获取产品类型列表",param = {
            @ApiDesc(name = "token",type = "header",desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败"),
            }

    )
    @Router(path = "/productType/List")
    @Proxy(target = AuthProxy.class)
    public Result list(RestForm form){
        ProductType productType = new ProductType();
        List<ProductType> list = productTypeDao.list(productType, ProductType.class);
        return new Result(list);
    }

}
