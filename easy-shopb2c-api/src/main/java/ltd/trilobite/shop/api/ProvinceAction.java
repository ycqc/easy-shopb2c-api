package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.dao.ProvinceDao;
import ltd.trilobite.shop.dao.entry.Province;

@ApiDesc(desc = "4014-省份")
@RestService
public class ProvinceAction {
    ProvinceDao provinceDao=new ProvinceDao();
    @ApiDoc(title = "单表查询",
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "代码"),
                    @ApiDesc(name = "name", type = "string", desc = "名称"),

            }
    )
    @Router(path = "/province/List",method = Router.Action.Get)
    public Result findList(RestForm form, @Param Province province) {
       return new Result(provinceDao.list(province,Province.class));
    }

}
