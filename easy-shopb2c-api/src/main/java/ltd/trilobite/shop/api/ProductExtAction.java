package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.ProductType;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.dao.*;
import ltd.trilobite.shop.dao.entry.MemberProduct;
import ltd.trilobite.shop.dao.entry.ProductExt;
import ltd.trilobite.shop.dao.entry.ProductReExt;
import ltd.trilobite.shop.dao.entry.ProductRes;

@ApiDesc(desc="5001-产品拓展信息")
@RestService
public class ProductExtAction {
    MemberProductDao memberProductDao = new MemberProductDao();
    ProductExtDao productExtDao = new ProductExtDao();
    ProductReExtDao productReExtDao = new ProductReExtDao();
    ProductResDao productResDao = new ProductResDao();

    @ApiDoc(title = "查询单个", param = {
            //商品信息
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "title", type = "string", desc = "商品标题"),
                    @ApiDesc(name = "title1", type = "string", desc = "商品副标题"),
                    @ApiDesc(name = "smailImage", type = "string", desc = "商品图片"),
                    @ApiDesc(name = "introlUrl", type = "string", desc = "商品简介URL"),
                    @ApiDesc(name = "inPrice", type = "string", desc = "进货价格"),
                    @ApiDesc(name = "selfProfit", type = "string", desc = "利润"),
                    @ApiDesc(name = "rewardPrice", type = "string", desc = "用于奖励的部分")
            }

    )
    @Router(path = "/productExt/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form) {
        ProductExt productExt = new ProductExt();
        productExt.setMemberProductId(Long.parseLong(form.get("memberProductId")));
        return new Result(productExtDao.findOne(productExt, ProductExt.class));
    }

    @ApiDoc(title = "保存普通产品", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "name", type = "string", desc = "名称"),
            @ApiDesc(name = "onOff", type = "string", desc = "是否上下架 0-上架 1-下架"),
            @ApiDesc(name = "price", type = "string", desc = "价格"),
            @ApiDesc(name = "htmlHeight", type = "string", desc = "html高度"),
            //拓展信息
            @ApiDesc(name = "title", type = "string", desc = "商品标题"),
            @ApiDesc(name = "title1", type = "string", desc = "商品副标题"),
            @ApiDesc(name = "smailImage", type = "string", desc = "商品图片"),
            @ApiDesc(name = "introlUrl", type = "string", desc = "商品简介URL"),
            @ApiDesc(name = "inPrice", type = "string", desc = "进货价格"),
            @ApiDesc(name = "selfProfit", type = "string", desc = "利润"),
            @ApiDesc(name = "rewardPrice", type = "string", desc = "用于奖励的部分"),

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败"),
            }

    )
    @Router(path = "/productExt/save")
    @Proxy(target = AuthProxy.class)
    public Result save(RestForm form, @Param MemberProduct memberProduct, @Param ProductExt productExt) {
        memberProduct.setName(productExt.getTitle());
        memberProduct.setProductTypeId(ProductType.GOODS.getValue());
        if(memberProduct.getMemberProductId() == null){
            Long memberProductId = memberProductDao.getSeq("member_product");
            memberProduct.setMemberProductId(memberProductId);
            productExt.setMemberProductId(memberProductId);
            memberProductDao.add(memberProduct);
            productExtDao.add(productExt);
        }else{
            productExt.setMemberProductId(memberProduct.getMemberProductId());
            memberProductDao.update(memberProduct);
            productExtDao.update(productExt);
        }
        return Response.SUCCESS;
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败"),
            }
    )
    @Router(path = "/productExt/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form) {
        Long memberProductId = Long.parseLong(form.get("memberProductId"));

        MemberProduct memberProduct = new MemberProduct();
        memberProduct.setMemberProductId(memberProductId);

        ProductExt productExt = new ProductExt();
        productExt.setMemberProductId(memberProductId);

        ProductReExt productReExt = new ProductReExt();
        productReExt.setMemberProductId(memberProductId);

        ProductRes productRes = new ProductRes();
        productRes.setMemberProductId(memberProductId);

        productExtDao.del(productExt);
        productReExtDao.del(productReExt);
        productResDao.del(productRes);
        memberProductDao.del(memberProduct);

        return Response.SUCCESS;
    }



    @ApiDoc(title = "查询商品列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "name", type = "string", desc = "名称"),
                    @ApiDesc(name = "onOff", type = "string", desc = "是否上下架 0-上架 1-下架"),
                    @ApiDesc(name = "price", type = "string", desc = "价格"),
                    @ApiDesc(name = "htmlHeight", type = "string", desc = "html高度"),

                    //拓展信息
                    @ApiDesc(name = "title", type = "string", desc = "标题"),
                    @ApiDesc(name = "title1", type = "string", desc = "副标题"),
                    @ApiDesc(name = "smailImage", type = "string", desc = "商品图片"),
                    @ApiDesc(name = "introlUrl", type = "string", desc = "商品简介URL"),
                    @ApiDesc(name = "inPrice", type = "string", desc = "进货价格"),
                    @ApiDesc(name = "selfProfit", type = "string", desc = "利润"),
                    @ApiDesc(name = "rewardPrice", type = "string", desc = "用于奖励的部分"),

                    //复消信息
                    @ApiDesc(name = "reTitle", type = "string", desc = "复消-标题"),
                    @ApiDesc(name = "reTitle1", type = "string", desc = "复消-副标题"),
                    @ApiDesc(name = "reInPrice", type = "string", desc = "复消-进货价格"),
                    @ApiDesc(name = "reSelfProfit", type = "string", desc = "复消-利润"),
                    @ApiDesc(name = "reRewardPrice", type = "string", desc = "复消-用于奖励的部分"),
                    @ApiDesc(name = "rePrice", type = "string", desc = "复消-价格"),
            }

    )
    @Router(path = "/productExt/list")
    @Proxy(target = AuthProxy.class)
    public Result list(RestForm form) {
        return new Result(productExtDao.findList());
    }

    @ApiDoc(title = "[前台]查询产品列表", param = {
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "title", type = "string", desc = "标题"),
                    @ApiDesc(name = "title1", type = "string", desc = "副标题"),
                    @ApiDesc(name = "price", type = "string", desc = "价格"),
                    @ApiDesc(name = "reTitle", type = "string", desc = "复消-标题"),
                    @ApiDesc(name = "reTitle1", type = "string", desc = "复消-副标题"),
                    @ApiDesc(name = "rePrice", type = "string", desc = "复消-价格"),
                    @ApiDesc(name = "smailImage", type = "string", desc = "商品图片"),
                    @ApiDesc(name = "isReConsumption", type = "string", desc = "复消标识 1-非复消 2-复消"),
            }

    )
    @Router(path = "/productExt/myGoodsList")
    @Proxy(target = AuthProxy.class)
    public Result myGoodsList(RestForm form) {
        return productExtDao.myGoodsList(form);
    }

    @ApiDoc(title = "查询商品详细", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "memberProductId", type = "string", desc = "产品ID"),
                    @ApiDesc(name = "name", type = "string", desc = "名称"),
                    @ApiDesc(name = "title", type = "string", desc = "标题"),
                    @ApiDesc(name = "title1", type = "string", desc = "副标题"),
                    @ApiDesc(name = "price", type = "string", desc = "价格"),
                    @ApiDesc(name = "smailImage", type = "string", desc = "商品图片"),
                    @ApiDesc(name = "introlUrl", type = "string", desc = "商品简介URL"),
                    @ApiDesc(name = "htmlHeight", type = "string", desc = "html高度"),
                    @ApiDesc(name = "resources", type = "string", desc = "资源数组"),
            }

    )
    @Router(path = "/productExt/detail")
    @Proxy(target = AuthProxy.class)
    public Result detail(RestForm form) {
        return new Result(productExtDao.findDetail(Long.parseLong(form.getHeader().get("personId")), Long.parseLong(form.get("memberProductId"))));
    }

}
