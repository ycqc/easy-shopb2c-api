package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.dao.StreetDao;
import ltd.trilobite.shop.dao.entry.Street;

@ApiDesc(desc = "4015-街道（乡镇）")
@RestService
public class StreetAction {
    StreetDao streetDao=new StreetDao();
    @ApiDoc(title = "单表查询",param = {

            @ApiDesc(name = "areacode",type = "string",desc = "区县代码"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "代码"),
                    @ApiDesc(name = "name", type = "string", desc = "名称"),
                    @ApiDesc(name = "areacode", type = "string", desc = "区县"),
                    @ApiDesc(name = "citycode", type = "string", desc = "城市代码"),
                    @ApiDesc(name = "provincecode", type = "string", desc = "省代码"),
            }
    )
    @Router(path = "/street/List",method = Router.Action.Get)

    public Result findList(RestForm form, @Param Street street) {
       return new Result(streetDao.list(street,Street.class));
    }

}
