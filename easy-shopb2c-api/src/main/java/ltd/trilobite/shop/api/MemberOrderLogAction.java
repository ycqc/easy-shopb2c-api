package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.dao.MemberOrderLogDao;

@ApiDesc(desc = "3003-订单操作日志")
@RestService
public class MemberOrderLogAction {
    MemberOrderLogDao memberOrderLogDao = new MemberOrderLogDao();

    @ApiDoc(title = "订单操作日志列表", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "memberOrderLogId", type = "string", desc = "日志ID"),
                    @ApiDesc(name = "orderId", type = "string", desc = "订单ID"),
                    @ApiDesc(name = "status", type = "string", desc = "订单状态"),
                    @ApiDesc(name = "createPerson", type = "string", desc = "创建人"),
                    @ApiDesc(name = "personName", type = "string", desc = "姓名"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
            }
    )
    @Router(path = "/memberOrderLog/list", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result list(RestForm form) {
        return new Result( memberOrderLogDao.findAll(Long.parseLong(form.get("orderId"))));
    }

}
