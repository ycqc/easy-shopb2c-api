package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.dao.CityDao;
import ltd.trilobite.shop.dao.entry.City;

@ApiDesc(desc = "4013-市")
@RestService
public class CityAction {
    CityDao areaDao=new CityDao();

    @ApiDoc(title = "单表查询",param = {

            @ApiDesc(name = "provincecode",type = "string",desc = "省代码"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "代码"),
                    @ApiDesc(name = "name", type = "string", desc = "名称"),
                    @ApiDesc(name = "provincecode", type = "string", desc = "省代码"),
            }
    )
    @Router(path = "/city/List",method = Router.Action.Get)
    public Result findList(RestForm form, @Param City city) {
       return new Result(areaDao.list(city,City.class));
    }

}
