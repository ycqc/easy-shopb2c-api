package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.common.Response;
import ltd.trilobite.shop.dao.LogisticsCompanyDao;
import ltd.trilobite.shop.dao.entry.LogisticsCompany;

@ApiDesc(desc = "4008-物流公司")
@RestService
public class LogisticsCompanyAction {

    LogisticsCompanyDao logisticsCompanyDao=new LogisticsCompanyDao();
    @ApiDoc(title = "新增",param = {
            @ApiDesc(name = "logisticsCompanyId", type = "string", desc = "编号"),
            @ApiDesc(name = "name", type = "string", desc = "公司名称"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
            },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }

    )
    @Router(path = "/logisticsCompany/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param LogisticsCompany logisticsCompany){
        logisticsCompanyDao.add(logisticsCompany);
        return Response.SUCCESS;
    }

    @ApiDoc(title = "修改",param = {
            @ApiDesc(name = "logisticsCompanyId",type = "string",desc = "编号"),
            @ApiDesc(name = "name",type = "string",desc = "公司名称"),
            @ApiDesc(name = "token",type = "header",desc = "密匙")
            },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/logisticsCompany/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form,  @Param LogisticsCompany logisticsCompany){
        logisticsCompanyDao.update(logisticsCompany);
        return Response.SUCCESS;
    }
    @ApiDoc(title = "删除",param = {
            @ApiDesc(name = "logisticsCompanyId",type = "string",desc = "编号"),
            @ApiDesc(name = "token",type = "header",desc = "密匙"),
            },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "响应码 200-成功 -1-失败"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }
    )
    @Router(path = "/logisticsCompany/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form,  @Param LogisticsCompany logisticsCompany){
        logisticsCompanyDao.del(logisticsCompany);
        return Response.SUCCESS;
    }

    @ApiDoc(title = "单表查询",param = {

            @ApiDesc(name = "token",type = "header",desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "logisticsCompanyId",type = "string",desc = "编号"),
                    @ApiDesc(name = "name",type = "string",desc = "公司名称"),
            }
    )
    @Router(path = "/logisticsCompany/List",method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form,  @Param LogisticsCompany logisticsCompany) {
        return new Result(logisticsCompanyDao.list(logisticsCompany,LogisticsCompany.class));
    }
}
