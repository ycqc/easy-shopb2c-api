package ltd.trilobite.shop.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;

import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.dao.AreaDao;
import ltd.trilobite.shop.dao.entry.Area;

@ApiDesc(desc = "4012-区县")
@RestService
public class AreaAction {
    AreaDao areaDao=new AreaDao();
    @ApiDoc(title = "单表查询",param = {

            @ApiDesc(name = "citycode",type = "string",desc = "市代码"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "代码"),
                    @ApiDesc(name = "name", type = "string", desc = "名称"),
                    @ApiDesc(name = "citycode", type = "string", desc = "城市代码"),
                    @ApiDesc(name = "provincecode", type = "string", desc = "省代码"),
            }
    )
    @Router(path = "/area/List",method = Router.Action.Get)
    public Result findList(RestForm form, @Param Area area) {
       return new Result(areaDao.list(area,Area.class));
    }

}
