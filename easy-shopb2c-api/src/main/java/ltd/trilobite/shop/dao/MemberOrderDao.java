package ltd.trilobite.shop.dao;

import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.jdbc.SqlParm;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.dao.entry.MemberOrder;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MemberOrderDao extends BaseDao<MemberOrder> {

    /**
     * 我的订单统计
     * @param personId
     * @return
     */
    public Map<String, Object> findMyOrderCount(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select \n" +
                "(select count(1) from member_order where person_id="+personId+" and order_state=1) as order_state1,\n" +
                "(select count(1) from member_order where person_id="+personId+" and order_state=2) as order_state2,\n" +
                "(select count(1) from member_order where person_id="+personId+" and order_state=3) as order_state3,\n" +
                "(select count(1) from member_order where person_id="+personId+" and is_send=1) as is_send1,\n" +
                "(select count(1) from member_order where person_id="+personId+" and is_send=2) as is_send2,\n" +
                "(select count(1) from member_order where person_id="+personId+" and is_send=3) as is_send3";
        return jdbcTemplet.getMap(sql);
    }


    /**
     * 分页查询我的订单
     * @param form
     * @return
     */
    public Result findMyOrders(RestForm form) {
 //       select
//                order_id,member_product_id,pay_credential,order_state,is_send,pay_price,integral_price,person_addr_id,
//                to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time
//                from member_order
//                where person_id=2 and order_state=1
//        order by mo.create_time desc

        JdbcTemplet jdbcTemplet = App.get("master");
        List<Object> params = new ArrayList<>();
        params.add(Long.parseLong(form.getHeader().get("personId")));
        SQL sql = new SQL();
        sql.select().cols("order_id, member_product_id, pay_credential, order_state, is_send, pay_price, product_title, num, integral_price, person_addr_id",
                "to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time")
                .from().t("member_order").where().fix("person_id=?");

        if(StringUtils.isNotBlank(form.get("orderState"))){
            sql.fix(" and order_state=?");
            params.add(Integer.parseInt(form.get("orderState")));
        }

        if(StringUtils.isNotBlank(form.get("isSend"))){
            sql.fix(" and is_send=?");
            params.add(Integer.parseInt(form.get("isSend")));
        }

        SQL countsql = new SQL();
        countsql.select().cols("count(1)").from(sql.toString(), "a");

        sql.order("create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, params.toArray(new Object[]{}));

    }

    public Result findNaviList(RestForm form) {
//        select mo.*, mp.name as product_name,p.name as person_name, p.phone_num,pe.title,pe.title1,pr.title,pr.title1 from
//                (
//                        select
//                        order_id,member_product_id,person_id,pay_credential,order_state,is_send,pay_price,integral_price,person_addr_id,
//                        to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time,is_re_consumption
//                        from member_order
//                        where person_id=2 and order_state=1
//                ) mo
//        left join member_product mp on mp.member_product_id=mo.member_product_id
//        left join person p on p.person_id=mo.person_id
//        left join product_ext pe on pe.member_product_id=mo.member_product_id
//        left join product_re_ext pr on pr.member_product_id=mo.member_product_id
//        order by mo.create_time desc


        JdbcTemplet jdbcTemplet = App.get("master");
        List<Object> params = new ArrayList<>();
        SQL orderSql = new SQL();
        orderSql.select().cols("order_id,member_product_id,person_id,pay_credential,order_state,is_send,pay_price,product_title, num,integral_price,person_addr_id",
                "to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time","is_re_consumption","receiver_addr","receiver_person","receiver_phonenum")
                .from().t("member_order").where(true);

        if(StringUtils.isNotBlank(form.get("orderId"))){
            orderSql.fix(" and order_id=?");
            params.add(Long.parseLong(form.get("orderId")));
        }

        if(StringUtils.isNotBlank(form.get("orderState"))){
            orderSql.fix(" and order_state=?");
            params.add(Integer.parseInt(form.get("orderState")));
        }

        if(StringUtils.isNotBlank(form.get("isSend"))){
            orderSql.fix(" and is_send=?");
            params.add(Integer.parseInt(form.get("isSend")));
        }

        if(StringUtils.isNotBlank(form.get("receiverPerson"))){
            orderSql.fix(" and receiver_person=?");
            params.add(Integer.parseInt(form.get("receiverPerson")));
        }

        if(StringUtils.isNotBlank(form.get("receiverPhonenum"))){
            orderSql.fix(" and receiver_phonenum=?");
            params.add(Integer.parseInt(form.get("receiverPhonenum")));
        }

        SQL sql = new SQL();
        sql.select().cols("mo.*, mp.name as product_name,p.name as person_name, p.phone_num,pe.title,pe.title1,pr.title as re_title,pr.title1 as re_title1",
                "li.logistics_invoice_id,li.num as logistics_num,li.price as logistics_price, lc.name as logistics_name,lc.logistics_company_id")
                .from().subsql(orderSql, "mo")
                .leftJoin().fix("member_product mp on mp.member_product_id=mo.member_product_id")
                .leftJoin().fix("product_ext pe on pe.member_product_id=mo.member_product_id")
                .leftJoin().fix("product_re_ext pr on pr.member_product_id=mo.member_product_id")
                .leftJoin().fix("logistics_invoice li on li.order_id=mo.order_id")
                .leftJoin().fix("logistics_company lc on lc.logistics_company_id=li.logistics_company_id")
                .leftJoin().fix("person p on p.person_id=mo.person_id")
                .where(true);

        if (StringUtils.isNotBlank(form.get("personName"))) {
            sql.and(new SqlParm("p.name").like().v("?"));
            params.add(form.get("personName") + "%");
        }

        if (StringUtils.isNotBlank(form.get("phoneNum"))) {
            sql.and(new SqlParm("p.phone_num").eq().v("?"));
            params.add(form.get("phoneNum"));
        }

        SQL countsql = new SQL();
        countsql.select().cols("count(1)").from(sql.toString(), "a");

        sql.order("mo.create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, params.toArray());
    }

    public MemberOrder findById(Long orderId) {
        MemberOrder memberOrder = new MemberOrder();
        memberOrder.setOrderId(orderId);
        return findOne(memberOrder, MemberOrder.class);
    }

    /**
     * 查询订单详细信息
     * @param orderId
     * @return
     */
    public Map<String, Object> findDetail(Long orderId) {
//
//        select mo.*,li.num as logistics_num,li.price as logistics_price, lc.name as logistics_name from
//        (
//                select order_id,member_product_id,pay_credential,order_state,is_send,pay_price,product_title, num,integral_price,person_addr_id,
//                to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time
//        from member_order where order_id=1
//) mo
//        left join logistics_invoice li on li.order_id=mo.order_id
//        left join logistics_company lc on lc.logistics_company_id=li.logistics_company_id

        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select mo.*,li.num as logistics_num,li.price as logistics_price, lc.name as logistics_name from \n" +
                "(\n" +
                "	select order_id,member_product_id,pay_credential,order_state,is_send,pay_price,product_title, num,integral_price,person_addr_id,receiver_addr,receiver_person,receiver_phonenum,\n" +
                "	to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time\n" +
                "	from member_order where order_id=?\n" +
                ") mo\n" +
                "left join logistics_invoice li on li.order_id=mo.order_id\n" +
                "left join logistics_company lc on lc.logistics_company_id=li.logistics_company_id";
        return jdbcTemplet.getMap(sql, orderId);
    }

    /**
     * 获取已支付非复消订单数量
     * @param personId
     * @return
     */
    public long getNoReConsumptionOrderCount(Long personId, Long memberProductId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select count(1) from member_order where person_id=? and member_product_id=? and is_re_consumption=1 and order_state in (2,3)",
                Long.class, personId, memberProductId);
    }

    /**
     * 获取会员有效的订单数量
     * @return
     */
    public long getValidOrderCount(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select count(1) from member_order where person_id=? and order_state in (2,3)",
                Long.class, personId);
    }

    /**
     *
     * 获取会员待付款的订单数量
     * @param personId
     * @return
     */
    public long getPayNoOrderCount(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select count(1) from member_order where person_id=? and order_state=1",
                Long.class, personId);
    }

    /**
     * 更新订单状态
     * @param orderId
     * @param orderState
     */
    public void updateOrderState(Long orderId, Integer orderState) {
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.execute("update member_order set order_state=? where order_id=?", orderState, orderId);
    }

    /**
     * 更新物流状态
     * @param orderId
     * @param shipState
     */
    public void updateShipState(Long orderId, Integer shipState) {
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.execute("update member_order set is_send=? where order_id=?", shipState, orderId);
    }
}
