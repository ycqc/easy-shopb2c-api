package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "recommend_reaward_person_config")
public class RecommendReawardPersonConfig {
  @Id
  private Long recommendReawardPersonConfigId;
  private Integer personNum;
  private Integer layer;


  public Long getRecommendReawardPersonConfigId() {
    return recommendReawardPersonConfigId;
  }

  public void setRecommendReawardPersonConfigId(Long recommendReawardPersonConfigId) {
    this.recommendReawardPersonConfigId = recommendReawardPersonConfigId;
  }


  public Integer getPersonNum() {
    return personNum;
  }

  public void setPersonNum(Integer personNum) {
    this.personNum = personNum;
  }


  public Integer getLayer() {
    return layer;
  }

  public void setLayer(Integer layer) {
    this.layer = layer;
  }

}
