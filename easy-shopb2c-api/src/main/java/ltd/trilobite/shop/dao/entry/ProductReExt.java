package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "product_re_ext")
public class ProductReExt {

  private String title;
  private String title1;
  private Double inPrice;
  private Double selfProfit;
  private Double rewardPrice;
  @Id
  private Long memberProductId;
  private Double price;


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }


  public String getTitle1() {
    return title1;
  }

  public void setTitle1(String title1) {
    this.title1 = title1;
  }


  public Double getInPrice() {
    return inPrice;
  }

  public void setInPrice(Double inPrice) {
    this.inPrice = inPrice;
  }


  public Double getSelfProfit() {
    return selfProfit;
  }

  public void setSelfProfit(Double selfProfit) {
    this.selfProfit = selfProfit;
  }


  public Double getRewardPrice() {
    return rewardPrice;
  }

  public void setRewardPrice(Double rewardPrice) {
    this.rewardPrice = rewardPrice;
  }


  public Long getMemberProductId() {
    return memberProductId;
  }

  public void setMemberProductId(Long memberProductId) {
    this.memberProductId = memberProductId;
  }


  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

}
