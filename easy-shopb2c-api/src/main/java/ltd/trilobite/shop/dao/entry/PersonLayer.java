package ltd.trilobite.shop.dao.entry;

public class PersonLayer {
    private Long memberId;
    private Long parentId;
    private Integer depth;

    public Integer getNextPersonNum() {
        return nextPersonNum;
    }

    public void setNextPersonNum(Integer nextPersonNum) {
        this.nextPersonNum = nextPersonNum;
    }

    private Integer nextPersonNum;

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }
}
