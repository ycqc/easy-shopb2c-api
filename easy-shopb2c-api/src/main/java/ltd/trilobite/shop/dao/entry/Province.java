package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "province")
public class Province {

  private String code;
  private String name;


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
