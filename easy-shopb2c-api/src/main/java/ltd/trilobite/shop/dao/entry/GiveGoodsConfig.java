package ltd.trilobite.shop.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "give_goods_config")
public class GiveGoodsConfig {
    @Id(type = IdType.Seq)
    private Long giveGoodsConfigId;
    private Long productId;
    private String goodName;
    private Double num;
    private String unit;
    private String code;
    private String goodsCode;


    public Long getGiveGoodsConfigId() {
        return giveGoodsConfigId;
    }

    public void setGiveGoodsConfigId(Long giveGoodsConfigId) {
        this.giveGoodsConfigId = giveGoodsConfigId;
    }


    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }


    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }


    public Double getNum() {
        return num;
    }

    public void setNum(Double num) {
        this.num = num;
    }


    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

}
