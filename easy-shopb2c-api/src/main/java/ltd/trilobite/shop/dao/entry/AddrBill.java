package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "addr_bill")
public class AddrBill {
  @Id(type=IdType.Seq)
  private Long addrBillId;
  private Double num;
  private Long personAddrAwardConfigId;
  private java.sql.Timestamp createTime;

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  private Long orderId;

  public Long getAddrBillId() {
    return addrBillId;
  }

  public void setAddrBillId(Long addrBillId) {
    this.addrBillId = addrBillId;
  }


  public Double getNum() {
    return num;
  }

  public void setNum(Double num) {
    this.num = num;
  }


  public Long getPersonAddrAwardConfigId() {
    return personAddrAwardConfigId;
  }

  public void setPersonAddrAwardConfigId(Long personAddrAwardConfigId) {
    this.personAddrAwardConfigId = personAddrAwardConfigId;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }

}
