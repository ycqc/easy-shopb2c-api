package ltd.trilobite.shop.dao;
import ltd.trilobite.shop.dao.entry.PersonAddrAwardConfig;

public class PersonAddrAwardConfigDao extends BaseDao<PersonAddrAwardConfig>{
    public long add(String addrCode,String addrName){
        PersonAddrAwardConfig personAddrAwardConfig=new PersonAddrAwardConfig();
        personAddrAwardConfig.setAddrCode(addrCode);

        PersonAddrAwardConfig personAddrAwardConfig1=super.findOne(personAddrAwardConfig,PersonAddrAwardConfig.class);
        if(personAddrAwardConfig1!=null){
            return personAddrAwardConfig1.getPersonAddrAwardConfigId();
        }else{
            Long s=super.getSeq("person_addr_award_config");
            personAddrAwardConfig.setPersonAddrAwardConfigId(s);
            personAddrAwardConfig.setAddrName(addrName);
            super.add(personAddrAwardConfig);
            return s;
        }
    }
}
