package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "person_addr")
public class PersonAddr {
  @Id(type= IdType.Seq)
  private Integer personAddrId;
  private String addrDetail;
  private String phoneNum;
  private String revPersonName;
  private Long personId;
  private String province;
  private String city;
  private String area;
  private String street;
  private Integer isDefault;


  public Integer getPersonAddrId() {
    return personAddrId;
  }

  public void setPersonAddrId(Integer personAddrId) {
    this.personAddrId = personAddrId;
  }


  public String getAddrDetail() {
    return addrDetail;
  }

  public void setAddrDetail(String addrDetail) {
    this.addrDetail = addrDetail;
  }


  public String getPhoneNum() {
    return phoneNum;
  }

  public void setPhoneNum(String phoneNum) {
    this.phoneNum = phoneNum;
  }


  public String getRevPersonName() {
    return revPersonName;
  }

  public void setRevPersonName(String revPersonName) {
    this.revPersonName = revPersonName;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }


  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }


  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }


  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public Integer getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Integer isDefault) {
    this.isDefault = isDefault;
  }
}
