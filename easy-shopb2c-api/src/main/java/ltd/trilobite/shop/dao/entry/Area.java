package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "area")
public class Area {

  private String code;
  private String name;
  private String citycode;
  private String provincecode;


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getCitycode() {
    return citycode;
  }

  public void setCitycode(String citycode) {
    this.citycode = citycode;
  }


  public String getProvincecode() {
    return provincecode;
  }

  public void setProvincecode(String provincecode) {
    this.provincecode = provincecode;
  }

}
