package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "logistics_company")
public class LogisticsCompany {
  @Id
  private Long logisticsCompanyId;
  private String name;


  public Long getLogisticsCompanyId() {
    return logisticsCompanyId;
  }

  public void setLogisticsCompanyId(Long logisticsCompanyId) {
    this.logisticsCompanyId = logisticsCompanyId;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
