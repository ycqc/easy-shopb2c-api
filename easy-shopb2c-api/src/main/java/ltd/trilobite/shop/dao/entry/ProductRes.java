package ltd.trilobite.shop.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name="product_res")
public class ProductRes {
  @Id(type = IdType.Seq)
  private Long productResId;
  private String url;
  private String extType;
  private Long memberProductId;


  public Long getProductResId() {
    return productResId;
  }

  public void setProductResId(Long productResId) {
    this.productResId = productResId;
  }


  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }


  public String getExtType() {
    return extType;
  }

  public void setExtType(String extType) {
    this.extType = extType;
  }


  public Long getMemberProductId() {
    return memberProductId;
  }

  public void setMemberProductId(Long memberProductId) {
    this.memberProductId = memberProductId;
  }

}
