package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "village")
public class Village {
  @Id
  private String code;
  private String name;
  private String streetcode;
  private String provincecode;
  private String citycode;
  private String areacode;


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getStreetcode() {
    return streetcode;
  }

  public void setStreetcode(String streetcode) {
    this.streetcode = streetcode;
  }


  public String getProvincecode() {
    return provincecode;
  }

  public void setProvincecode(String provincecode) {
    this.provincecode = provincecode;
  }


  public String getCitycode() {
    return citycode;
  }

  public void setCitycode(String citycode) {
    this.citycode = citycode;
  }


  public String getAreacode() {
    return areacode;
  }

  public void setAreacode(String areacode) {
    this.areacode = areacode;
  }

}
