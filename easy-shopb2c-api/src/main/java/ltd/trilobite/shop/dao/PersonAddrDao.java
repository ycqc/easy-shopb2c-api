package ltd.trilobite.shop.dao;

import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.jdbc.SqlParm;
import ltd.trilobite.shop.dao.entry.PersonAddr;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonAddrDao extends BaseDao<PersonAddr>{


    /**
     * 获取收货人详细信息
     * @param personAddrId
     * @return
     */
    public Map<String, String> getReceiverInfo(Long personAddrId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        SQL subsql = new SQL();
        subsql.select().cols("person_addr_id, addr_detail, phone_num, rev_person_name","is_default",
                "province as province_code", "city as city_code", "area as area_code","street as street_code")
                .from().t("person_addr")
                .where().fix("person_addr_id=?");

        SQL sql = new SQL();
        sql.select().cols("t.*,p.name as province_name,c.name as city_name, a.name as area_name, s.name as street_name")
                .from().subsql(subsql, "t")
                .fix(" left join province p on p.code=t.province_code")
                .fix(" left join city c on c.code=t.city_code")
                .fix(" left join area a on a.code=t.area_code")
                .fix(" left join street s on s.code=t.street_code");

       Map<String, Object> map = jdbcTemplet.getMap(sql.toString(), personAddrId);
       StringBuilder builder = new StringBuilder();
       builder.append(map.get("provinceName"))
               .append(map.get("cityName"))
               .append(map.get("areaName"))
               .append(map.get("streetName"))
               .append(map.get("addrDetail"));
       Map<String, String> result = new HashMap<>();
       result.put("receiverAddr", builder.toString());
       result.put("receiverPerson",String.valueOf(map.get("revPersonName")));
       result.put("receiverPhonenum", String.valueOf(map.get("phoneNum")));
       return result;
    }


    public void setDefault(Long personId, Long personAddrId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.execute("update person_addr set is_default=0 where person_id=?", personId);
        jdbcTemplet.execute("update person_addr set is_default=1 where person_addr_id=?", personAddrId);
    }

    public List<Map<String,Object>> myList(Long personId){
        JdbcTemplet jdbcTemplet = App.get("master");
        SQL personBankSql=new SQL();
        personBankSql.select().cols("*").from().t("person_addr").where(true).and(new SqlParm("person_id").eq().v("?"));

        SQL sql=new SQL();
        sql.select().cols("pa.*","p.name as province_name","c.name as city_name","a.name as area_name","s.name as street_name")
                .from(personBankSql.toString(),"pa")
                .leftJoin().t("province p")
                .on(new SqlParm("pa.province").eq().v("p.code"))
                .leftJoin().t("city c")
                .on(new SqlParm("pa.city").eq().v("c.code"))
                 .leftJoin().t("area a")
                .on(new SqlParm("pa.area").eq().v("a.code"))
                .leftJoin().t("street s")
                .on(new SqlParm("pa.street").eq().v("s.code"));

        return jdbcTemplet.findList(sql.toString(),personId);
    }
    public Map<String, Object> findDetail(Long personAddrId) {
//        select t.*,p.name as province_name,c.name as city_name, a.name as area_name, s.name as street_name
//        from
//                (
//                        SELECT person_addr_id, addr_detail, phone_num, rev_person_name,
//                        province as province_code,
//                        city as city_code,
//                        area as area_code,
//                        street as street_code
//                        FROM person_addr
//                        WHERE person_addr_id=1 and person_id=1
//                ) t
//        left join province p on p.code=t.province_code
//        left join city c on c.code=t.city_code
//        left join area a on a.code=t.area_code
//        left join street s on s.code=t.street_code
        JdbcTemplet jdbcTemplet = App.get("master");
        SQL subsql = new SQL();
        subsql.select().cols("person_addr_id, addr_detail, phone_num, rev_person_name","is_default",
                "province as province_code", "city as city_code", "area as area_code","street as street_code")
                .from().t("person_addr")
                .where().fix("person_addr_id=?");

        SQL sql = new SQL();
        sql.select().cols("t.*,p.name as province_name,c.name as city_name, a.name as area_name, s.name as street_name")
           .from().subsql(subsql, "t")
           .fix(" left join province p on p.code=t.province_code")
           .fix(" left join city c on c.code=t.city_code")
           .fix(" left join area a on a.code=t.area_code")
           .fix(" left join street s on s.code=t.street_code");

        Map<String, Object> map = jdbcTemplet.getMap(sql.toString(), personAddrId);

        Map<String, Object> row = new HashMap<>();
        row.put("code", map.get("provinceCode"));
        row.put("name", map.get("provinceName"));
        map.put("province", row);

        row = new HashMap<>();
        row.put("code", map.get("cityCode"));
        row.put("name", map.get("cityName"));
        map.put("city", row);

        row = new HashMap<>();
        row.put("code", map.get("areaCode"));
        row.put("name", map.get("areaName"));
        map.put("area", row);

        row = new HashMap<>();
        row.put("code", map.get("streetCode"));
        row.put("name", map.get("streetName"));
        map.put("street", row);

        map.remove("provinceCode");
        map.remove("provinceName");
        map.remove("cityCode");
        map.remove("cityName");
        map.remove("areaCode");
        map.remove("areaName");
        map.remove("streetCode");
        map.remove("streetName");

        return map;
    }

    public Map<String, Object> findDefault(Long personId) {
//        select t.*,p.name as province_name,c.name as city_name, a.name as area_name, s.name as street_name
//        from
//                (
//                        SELECT person_addr_id, addr_detail, phone_num, rev_person_name,
//                        province as province_code,
//                        city as city_code,
//                        area as area_code,
//                        street as street_code
//                        FROM person_addr
//                        WHERE person_addr_id=1 and person_id=1
//                ) t
//        left join province p on p.code=t.province_code
//        left join city c on c.code=t.city_code
//        left join area a on a.code=t.area_code
//        left join street s on s.code=t.street_code
        JdbcTemplet jdbcTemplet = App.get("master");
        SQL subsql = new SQL();
        subsql.select().cols("person_addr_id, addr_detail, phone_num, rev_person_name","is_default",
                "province as province_code", "city as city_code", "area as area_code","street as street_code")
                .from().t("person_addr")
                .where().fix("person_id=? and is_default=1");

        SQL sql = new SQL();
        sql.select().cols("t.*,p.name as province_name,c.name as city_name, a.name as area_name, s.name as street_name")
                .from().subsql(subsql, "t")
                .fix(" left join province p on p.code=t.province_code")
                .fix(" left join city c on c.code=t.city_code")
                .fix(" left join area a on a.code=t.area_code")
                .fix(" left join street s on s.code=t.street_code");

        Map<String, Object> map = jdbcTemplet.getMap(sql.toString(), personId);

        Map<String, Object> row = new HashMap<>();
        row.put("code", map.get("provinceCode"));
        row.put("name", map.get("provinceName"));
        map.put("province", row);

        row = new HashMap<>();
        row.put("code", map.get("cityCode"));
        row.put("name", map.get("cityName"));
        map.put("city", row);

        row = new HashMap<>();
        row.put("code", map.get("areaCode"));
        row.put("name", map.get("areaName"));
        map.put("area", row);

        row = new HashMap<>();
        row.put("code", map.get("streetCode"));
        row.put("name", map.get("streetName"));
        map.put("street", row);

        map.remove("provinceCode");
        map.remove("provinceName");
        map.remove("cityCode");
        map.remove("cityName");
        map.remove("areaCode");
        map.remove("areaName");
        map.remove("streetCode");
        map.remove("streetName");

        return map;
    }

}
