package ltd.trilobite.shop.dao;

import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.jdbc.SqlParm;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.dao.entry.MemberProduct;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MemberProductDao extends BaseDao<MemberProduct> {

    /**
     * 获取产品价格
     * @param memberProductId
     * @return
     */
    public Double getPrice(Long memberProductId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select price from member_product where member_product_id=?", BigDecimal.class, memberProductId).doubleValue();
    }

    public List<Map<String, Object>> myProductCount(long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select  mp.member_product_id,mp.name,m2.count,m2.count*mp.price as price from member_product mp left join\n" +
                "            (select member_product_id,count(1) from  member_order where person_id=" + personId + " group by member_product_id)\n" +
                "    m2 on mp.member_product_id = m2.member_product_id where mp.on_off=0";
        return jdbcTemplet.findList(sql);
    }

    public List<Map<String, Object>> findList() {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select mp.*,pe.* FROM\n" +
                "(\n" +
                "	select * \n" +
                "	from member_product\n" +
                "	where product_type_id=1\n" +
                ") mp\n" +
                "left join product_ext pe on pe.member_product_id=mp.member_product_id\n";

        return jdbcTemplet.findList(sql);
    }


    public Result myGoodsList(RestForm form){
        JdbcTemplet jdbcTemplet = App.get("master");
        List<Object> params = new ArrayList<>();
        SQL mp = new SQL();
        mp.select().cols("member_product_id, name, price")
                .from().t("member_product")
                .where().fix("on_off=0 and product_type_id=1 ");

        SQL countsql = new SQL();
        countsql.select().cols("count(1)").from(mp.toString(), "a");
        SQL sql = new SQL();
        sql.select().cols("mp.*","pe.*").from().subsql(mp,"mp").leftJoin().t("product_ext pe")
                .on(new SqlParm("pe.member_product_id").eq().v("mp.member_product_id"));
        sql.limit(form.get("start"), form.get("pageSize"));
        return jdbcTemplet.naviList(sql.toString(), countsql.toString(),null,params.toArray());
    }

    public MemberProduct findById(Long id){
        MemberProduct memberProduct = new MemberProduct();
        memberProduct.setMemberProductId(id);
        return findOne(memberProduct, MemberProduct.class);
    }

//    public Result findNaviList(RestForm form) {
//        JdbcTemplet jdbcTemplet = App.get("master");
//        List<Object> param = new ArrayList<>();
//        SQL sql = new SQL()
//                .select().cols("member_product_id, product_type_id, name, on_off, price")
//                .from().t("member_product")
//                .where(true);
//
//        if(StringUtils.isNotBlank(form.get("name"))){
//            sql.fix(" and name like ?");
//            param.add(form.get("name").trim() + "%");
//        }
//
//        if(StringUtils.isNotBlank(form.get("productTypeId"))){
//            sql.fix(" and product_type_id=?");
//            param.add(Long.parseLong(form.get("productTypeId")));
//        }
//
//        if(StringUtils.isNotBlank(form.get("onOff"))){
//            sql.fix(" and on_off=?");
//            param.add(Integer.parseInt(form.get("onOff")));
//        }
//
//        SQL countsql = new SQL().select().cols("count(1)").from(sql.toString(), "t");
//
//        sql.order("member_product_id").desc();
//        sql.limit(form.get("start"), form.get("pageSize"));
//
//        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, param.toArray());
//    }
}
