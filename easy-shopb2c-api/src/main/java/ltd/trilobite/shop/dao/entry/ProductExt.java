package ltd.trilobite.shop.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name="product_ext")
public class ProductExt {

  @Id(type = IdType.None)
  private Long memberProductId;
  private String title;
  private String title1;
  private String smailImage;
  private String introlUrl;
  private Double inPrice;
  private Double selfProfit;
  private Double rewardPrice;


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }


  public Long getMemberProductId() {
    return memberProductId;
  }

  public void setMemberProductId(Long memberProductId) {
    this.memberProductId = memberProductId;
  }


  public String getTitle1() {
    return title1;
  }

  public void setTitle1(String title1) {
    this.title1 = title1;
  }


  public String getSmailImage() {
    return smailImage;
  }

  public void setSmailImage(String smailImage) {
    this.smailImage = smailImage;
  }


  public String getIntrolUrl() {
    return introlUrl;
  }

  public void setIntrolUrl(String introlUrl) {
    this.introlUrl = introlUrl;
  }


  public Double getInPrice() {
    return inPrice;
  }

  public void setInPrice(Double inPrice) {
    this.inPrice = inPrice;
  }


  public Double getSelfProfit() {
    return selfProfit;
  }

  public void setSelfProfit(Double selfProfit) {
    this.selfProfit = selfProfit;
  }


  public Double getRewardPrice() {
    return rewardPrice;
  }

  public void setRewardPrice(Double rewardPrice) {
    this.rewardPrice = rewardPrice;
  }

}
