package ltd.trilobite.shop.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

import java.util.Date;


@Table(name = "achievements_settlement")
public class AchievementsSettlement {
    @Id(type = IdType.Seq)
    private Long achievementsSettlementId;
    private Date startTime;
    private Date endTime;
    private Integer state;
    private String description;


    public Long getAchievementsSettlementId() {
        return achievementsSettlementId;
    }

    public void setAchievementsSettlementId(Long achievementsSettlementId) {
        this.achievementsSettlementId = achievementsSettlementId;
    }


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
