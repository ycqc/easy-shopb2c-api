package ltd.trilobite.shop.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

import java.util.Date;

@Table(name = "person_achievements")
public class PersonAchievements {
    @Id(type = IdType.Seq)
    private Long personAchievementsId;
    private Double num;
    private Long orderId;
    private Date createTime;
    private Long personId;
    private Long orderPersonId;


    public Long getPersonAchievementsId() {
        return personAchievementsId;
    }

    public void setPersonAchievementsId(Long personAchievementsId) {
        this.personAchievementsId = personAchievementsId;
    }


    public Double getNum() {
        return num;
    }

    public void setNum(Double num) {
        this.num = num;
    }


    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }


    public Long getOrderPersonId() {
        return orderPersonId;
    }

    public void setOrderPersonId(Long orderPersonId) {
        this.orderPersonId = orderPersonId;
    }

}
