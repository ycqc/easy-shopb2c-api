package ltd.trilobite.shop.dao;

import ltd.trilobite.shop.dao.entry.Bill;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.jdbc.SqlParm;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;
import ltd.trilobite.shop.dao.entry.GlobalSet;

import java.util.*;

public class BillDao extends BaseDao<Bill> {

    public Result findMyNaviList(Long PersonId, RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");

//        String sql="select b.state,b.description,b.num,to_char(b.create_time, 'YYYY-MM-DD HH12:MI:SS') as create_time,ba.label from (select * from bill where person_id="+PersonId+") as b left join (select code,label from base_data where type_id=1 ) as ba on b.code=ba.code order by b.create_time desc limit "+form.get("pageSize")+" offset "+form.get("start");
//
//
//        String countSql=" select count(1) from bill where person_id="+PersonId;
//        List<Object> param=new ArrayList<Object>();
//        return jdbcTemplet.naviList(sql,countSql,null,param.toArray());

        List<Object> param = new ArrayList<Object>();
        // String sql="select b.state,b.description,b.num,to_char(b.create_time, 'YYYY-MM-DD HH12:MI:SS') as create_time,ba.label from (select * from bill where person_id="+PersonId+") as b left join (select code,label from base_data where type_id=1 ) as ba on b.code=ba.code order by b.create_time desc limit "+form.get("pageSize")+" offset "+form.get("start");
        // String countSql=" select count(1) from bill where person_id="+PersonId;
        SQL billSql = new SQL();
        billSql.select().cols("*").from().t("bill").where(true);
        billSql.and(new SqlParm("person_id").eq().v("?"));
        param.add(PersonId);
        if (Util.isNotEmpty(form.get("state"))) {
            billSql.and(new SqlParm("state").eq().v("?"));
            param.add(Integer.parseInt(form.get("state")));
        }

        if (Util.isNotEmpty(form.get("actionCode"))) {
            billSql.and(new SqlParm("action_code").eq().v("?"));
            param.add(form.get("actionCode"));
        }

        if (Util.isNotEmpty(form.get("goodsCode"))) {
            billSql.and(new SqlParm("code").eq().v("?"));
            param.add(form.get("goodsCode"));
        }

        if (Util.isNotEmpty(form.get("orderId"))) {
            billSql.and(new SqlParm("src_id").eq().v("?"));
            param.add(Long.parseLong(form.get("orderId")));
        }


        SQL baseDateAction = new SQL();
        baseDateAction.select().cols("code", "label").from().t("base_data").where().add(true, "type_id", "3");

        SQL baseDateGoods = new SQL();
        baseDateGoods.select().cols("code", "label", "attr1").from().t("base_data").where().add(true, "type_id", "1");

        SQL personSql = new SQL();
        personSql.select().cols("*").from().t("person");

        SQL sql = new SQL();
        sql.select().cols(
                "b.bill_id", "b.state", "b.num", "b.src_id", "to_char(b.create_time, 'YYYY-MM-DD HH12:MI:SS') as create_time", "b.description"
                , "bda.label as action_name", "bdg.label as goods_name", "bdg.attr1 as unit "
        ).from(billSql.toString(), "b")
                .leftJoin().subsql(baseDateAction, "bda")
                .on(new SqlParm("b.action_code").eq().v("bda.code"))
                .leftJoin().subsql(baseDateGoods, "bdg")
                .on(new SqlParm("b.code").eq().v("bdg.code"))
                //.leftJoin().subsql(personSql,"p")
                //.on(new SqlParm("b.person_id").eq().v("p.person_id"))
                .where(true);


        SQL countSql = new SQL();
        countSql.select().cols(" count(1) ").from(billSql.toString(), "aa");
        sql.order("b.person_id", "b.create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(sql.toString(), countSql.toString(), null, param.toArray());
    }

    public void delActionSrcId(Long orderId, String action) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "delete from bill where src_id=? and action_code=?";
        List<Object> param = new ArrayList<>();
        param.add(orderId);
        param.add(action);
        jdbcTemplet.save(sql, param);
    }

//    public Double findBalance(Long personId) {
//        JdbcTemplet jdbcTemplet = App.get("master");
//        String sql = "select coalesce(sum(num),0) from bill where person_id=? and code='5' and state=3";
//        return jdbcTemplet.getObject(sql, java.math.BigDecimal.class, personId).doubleValue();
//    }

    /**
     * 查询剩余的抵扣余额
     * (订单抵扣=循环钱包+红包)
     * @param personId
     * @return
     */
    public Double findDeductBalance(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select coalesce(sum(num),0) from bill where person_id=? and code='6' and state=3";
        return jdbcTemplet.getObject(sql, java.math.BigDecimal.class, personId).doubleValue();
    }

    /**
     * 查询优程积分余额
     * (订单抵扣=循环钱包+红包)
     * @param personId
     * @return
     */
    public Double findYcJfBalance(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select coalesce(sum(num),0) from bill where person_id=? and code='6' and state=3";
        return jdbcTemplet.getObject(sql, java.math.BigDecimal.class, personId).doubleValue();
    }

    public Result findMyBillCount(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql1 = "select * from("
                + " select action_code,count(1) from bill where person_id=" + personId + "  group by action_code"
                + " ) as b right join( select label,code from base_data where type_id=3) bd on bd.code=b.action_code";

        String sql2 = "select b.code,b.sum,bd.label,bd.attr1 from("
                + "select code,sum(num) from bill where person_id=" + personId + "  group by code"
                + ") as b left join( select label,code,attr1 from base_data where type_id=1) bd on bd.code=b.code";

        String sql3 = "select count(1),state from bill where person_id=" + personId + " group by state";
        Map<String, Object> map = new HashMap<>();
        map.put("action", jdbcTemplet.findList(sql1));
        map.put("type", jdbcTemplet.findList(sql2));
        map.put("state", jdbcTemplet.findList(sql3));
        return new Result(map);
    }

    public Result findNaviList(RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        List<Object> param = new ArrayList<Object>();
        // String sql="select b.state,b.description,b.num,to_char(b.create_time, 'YYYY-MM-DD HH12:MI:SS') as create_time,ba.label from (select * from bill where person_id="+PersonId+") as b left join (select code,label from base_data where type_id=1 ) as ba on b.code=ba.code order by b.create_time desc limit "+form.get("pageSize")+" offset "+form.get("start");
        // String countSql=" select count(1) from bill where person_id="+PersonId;
        SQL billSql = new SQL();
        billSql.select().cols("*").from().t("bill").where(true);
        if (Util.isNotEmpty(form.get("state"))) {
            billSql.and(new SqlParm("state").eq().v("?"));
            param.add(Integer.parseInt(form.get("state")));
        }

        if (Util.isNotEmpty(form.get("keyword"))) {
            billSql.fix(" and person_id in (select person_id from person where name||phone_num like ? )");
            param.add("%" + form.get("keyword") + "%");
        }

        if (Util.isNotEmpty(form.get("actionCode"))) {
            billSql.and(new SqlParm("action_code").eq().v("?"));
            param.add(form.get("actionCode"));
        }

        if (Util.isNotEmpty(form.get("goodsCode"))) {
            billSql.and(new SqlParm("code").eq().v("?"));
            param.add(form.get("goodsCode"));
        }


        SQL baseDateAction = new SQL();
        baseDateAction.select().cols("code", "label").from().t("base_data").where().add(true, "type_id", "3");

        SQL baseDateGoods = new SQL();
        baseDateGoods.select().cols("code", "label", "attr1").from().t("base_data").where().add(true, "type_id", "1");

        SQL personSql = new SQL();
        personSql.select().cols("*").from().t("person");

        SQL sql = new SQL();
        sql.select().cols(
                "b.bill_id", "b.state", "b.num", "b.src_id", "to_char(b.create_time, 'YYYY-MM-DD HH12:MI:SS') as create_time", "b.description"
                , "bda.label as action_name", "bdg.label as goods_name", "bdg.attr1 as unit ", "p.name", "p.gender", "p.phone_num"
        ).from(billSql.toString(), "b")
                .leftJoin().subsql(baseDateAction, "bda")
                .on(new SqlParm("b.action_code").eq().v("bda.code"))
                .leftJoin().subsql(baseDateGoods, "bdg")
                .on(new SqlParm("b.code").eq().v("bdg.code"))
                .leftJoin().subsql(personSql, "p")
                .on(new SqlParm("b.person_id").eq().v("p.person_id"))
                .where(true);


        SQL countSql = new SQL();
        countSql.select().cols(" count(1) ").from(billSql.toString(), "aa");
        sql.order("b.person_id", "b.create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(sql.toString(), countSql.toString(), null, param.toArray());

    }

    public Integer convert(long personId, RestForm form,int p) {

        if(!form.get("targetCode").equals("1")&&!form.get("targetCode").equals("4")){
            return -1;//转换目标不支持
        }

        Double price=Double.parseDouble(form.get("num"));
        Double ptPrice=price*p/100;
        Double usePrice=price-ptPrice;

        Bill bill=new Bill();
        bill.setState(3);
        bill.setPersonId(personId);
        bill.setCode(6+"");
        bill.setNum(-usePrice);
        bill.setActionCode(13+"");
        bill.setCreateTime(new Date());
        bill.setDescription("转换扣除余额");
        Bill bill2=new Bill();
        bill2.setState(3);
        bill2.setPersonId(personId);
        bill2.setCode(6+"");
        bill2.setNum(-ptPrice);
        bill2.setActionCode(13+"");
        bill2.setCreateTime(new Date());
        bill2.setDescription("转换扣除平台费");

        Bill bill1=new Bill();
        bill1.setState(3);
        bill1.setPersonId(personId);
        bill1.setCode(form.get("targetCode"));
        bill1.setNum(usePrice);
        bill1.setActionCode(13+"");
        bill1.setCreateTime(new Date());
        bill1.setDescription("转换入账");

        List<Object> li=new ArrayList<>();
        li.add(bill);
        li.add(bill1);
        li.add(bill2);

        super.adds(li);
        return 200;


    }

    public void updateStateComplet(Long orderId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "update bill set state=3 where src_id=?";
        List<Object> param = new ArrayList<>();
        param.add(orderId);
        jdbcTemplet.save(sql, param);
    }
}
