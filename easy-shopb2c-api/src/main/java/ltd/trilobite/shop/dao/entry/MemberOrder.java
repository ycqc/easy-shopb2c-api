package ltd.trilobite.shop.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

import java.util.Date;

@Table(name = "member_order")
public class MemberOrder {
    @Id
    private Long orderId;
    private Long personId;
    private Long memberProductId;
    private String payCredential;
    private Date createTime;
    private Integer orderState;
    private Long auditPerson;
    private Integer isSend;
    private Double payPrice;
    private Double integralPrice;
    private Long personAddrId;
    private Integer isReConsumption;
    private String productTitle;
    private Integer num;
    private String receiverAddr;
    private String receiverPerson;
    private String receiverPhonenum;


    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }


    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }


    public Long getMemberProductId() {
        return memberProductId;
    }

    public void setMemberProductId(Long memberProductId) {
        this.memberProductId = memberProductId;
    }


    public String getPayCredential() {
        return payCredential;
    }

    public void setPayCredential(String payCredential) {
        this.payCredential = payCredential;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }


    public Long getAuditPerson() {
        return auditPerson;
    }

    public void setAuditPerson(Long auditPerson) {
        this.auditPerson = auditPerson;
    }

    public Integer getIsSend() {
        return isSend;
    }

    public void setIsSend(Integer isSend) {
        this.isSend = isSend;
    }

    public Double getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(Double payPrice) {
        this.payPrice = payPrice;
    }

    public Double getIntegralPrice() {
        return integralPrice;
    }

    public void setIntegralPrice(Double integralPrice) {
        this.integralPrice = integralPrice;
    }

    public Long getPersonAddrId() {
        return personAddrId;
    }

    public void setPersonAddrId(Long personAddrId) {
        this.personAddrId = personAddrId;
    }

    public Integer getIsReConsumption() {
        return isReConsumption;
    }

    public void setIsReConsumption(Integer isReConsumption) {
        this.isReConsumption = isReConsumption;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getReceiverAddr() {
        return receiverAddr;
    }

    public void setReceiverAddr(String receiverAddr) {
        this.receiverAddr = receiverAddr;
    }

    public String getReceiverPerson() {
        return receiverPerson;
    }

    public void setReceiverPerson(String receiverPerson) {
        this.receiverPerson = receiverPerson;
    }

    public String getReceiverPhonenum() {
        return receiverPhonenum;
    }

    public void setReceiverPhonenum(String receiverPhonenum) {
        this.receiverPhonenum = receiverPhonenum;
    }
}
