package ltd.trilobite.shop.dao;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.jdbc.SqlParm;
import ltd.trilobite.shop.dao.entry.MemberOrderLog;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class MemberOrderLogDao extends BaseDao<MemberOrderLog>{
    public void add(Long orderId, Long personId, Integer status) {
        MemberOrderLog log = new MemberOrderLog();
        log.setStatus(status);
        log.setOrderId(orderId);
        log.setCreatePerson(personId);
        log.setCreateTime(new Date());
        add(log);
    }


    public List<Map<String, Object>> findAll(Long orderId) {
//        select m.*, p.name as person_name
//        from
//        (
//                select member_order_log_id,order_id,status,create_person,
//                to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time
//                from member_order_log where order_id=12
//        ) m
//        left join person p on p.person_id=m.create_person

        SQL subsql = new SQL();
        subsql.select().cols("member_order_log_id,order_id,status,create_person,to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time")
                .from().t("member_order_log").where().fix("order_id=?");

        SQL sql = new SQL();
        sql.select().cols("m.*, p.name as person_name").from().subsql(subsql, "m")
                .leftJoin().fix("person p on p.person_id=m.create_person");

        JdbcTemplet jdbcTemplet = App.get("master");

        return jdbcTemplet.findList(sql.toString(), orderId);
    }
}
