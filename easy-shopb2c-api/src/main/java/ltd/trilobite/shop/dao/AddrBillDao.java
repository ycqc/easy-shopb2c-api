package ltd.trilobite.shop.dao;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.shop.dao.entry.AddrBill;

public class AddrBillDao extends BaseDao<AddrBill>{
    public void delForOrderId(Long OrderId){
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.execute("delete from addr_bill where order_id=?",OrderId);
    }
}
