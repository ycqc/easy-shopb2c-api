package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "recommend_reward_config")
public class RecommendRewardConfig {
  @Id
  private Long recommendRewardConfigId;
  private Integer layer;
  private Double proportion;


  public Long getRecommendRewardConfigId() {
    return recommendRewardConfigId;
  }

  public void setRecommendRewardConfigId(Long recommendRewardConfigId) {
    this.recommendRewardConfigId = recommendRewardConfigId;
  }


  public Integer getLayer() {
    return layer;
  }

  public void setLayer(Integer layer) {
    this.layer = layer;
  }


  public Double getProportion() {
    return proportion;
  }

  public void setProportion(Double proportion) {
    this.proportion = proportion;
  }

}
