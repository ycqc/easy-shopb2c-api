package ltd.trilobite.shop.dao;

import ltd.trilobite.shop.dao.entry.MemberStatus;
import ltd.trilobite.shop.dao.entry.PersonLayer;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import org.apache.commons.collections.ListUtils;

import java.util.*;
import java.util.stream.Collectors;


public class MemberStatusDao extends BaseDao<MemberStatus> {

    public Result findSubMember(Long personId, RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select p.name,p.gender,p.phone_num,p.member_type,to_char(p.create_time, 'YYYY-MM-DD HH12:MI:SS') as create_time,p.tream_num,p.tream_park from (select member_id  from member_status where parent_id=" + personId + ") as a left join person p on a.member_id=p.person_id order by p.create_time desc limit " + form.get("pageSize") + " offset " + form.get("start");
        String countSql = "select count(1)  from member_status where parent_id=" + personId;
        List<Object> param = new ArrayList<Object>();
        return jdbcTemplet.naviList(sql, countSql, null, param.toArray());
    }

    /**
     * 获取有业绩直推人数
     * @return
     */
    public Map<Long, Integer> getRecommendPersonCountMap(List<String> idList) {
//        select t.parent_id,count(t.parent_id) from
//                (
//                        select m.member_id,m.parent_id, sum(p.num) from
//                                (
//                                        select member_id,parent_id
//                                        from member_status where parent_id in (1)
//                                ) m
//                        left join person_achievements p on p.order_person_id=m.member_id
//                        group by m.member_id,m.parent_id
//                        having sum(p.num) > 0
//                ) t
//        group by t.parent_id
        String idStr = idList.stream().collect(Collectors.joining(",", "", ""));
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select t.parent_id,count(t.parent_id) from\n" +
                "(\n" +
                "    select m.member_id,m.parent_id, sum(p.num) from\n" +
                "		(\n" +
                "				select member_id,parent_id\n" +
                "				from member_status where parent_id in (" + idStr + ")\n" +
                "		 ) m\n" +
                "		left join person_achievements p on p.order_person_id=m.member_id\n" +
                "		group by m.member_id,m.parent_id\n" +
                "		having sum(p.num) > 0\n" +
                ") t\n" +
                "group by t.parent_id";
        List<Map<String, Object>> list = jdbcTemplet.findList(sql);
        Map<Long, Integer> map = list.stream().collect(Collectors.toMap(item -> Long.parseLong(item.get("parentId").toString()), item -> Integer.parseInt(item.get("count").toString())));
        return Collections.unmodifiableMap(map);
    }


    public List<PersonLayer> layerParent(PersonLayer personLayer) {
//        WITH RECURSIVE r AS
//        (
//                SELECT *,1 as depth FROM member_status WHERE member_id=2
//        union   ALL
//        SELECT t.*,r.depth+1 as depth FROM member_status t, r WHERE t.member_id= r.parent_id
//)
//        SELECT * FROM r where r.depth<=12 ORDER BY depth

        String sql = "WITH RECURSIVE r AS (\n" +
                "\n" +
                "    SELECT *,1 as depth FROM member_status WHERE member_id= ?\n" +
                "        union   ALL\n" +
                "        SELECT t.*,r.depth+1 as depth FROM member_status t, r WHERE t.member_id= r.parent_id\n" +
                "    )\n" +
                "\n" +
                "SELECT * FROM r where r.depth<=? ORDER BY depth";
        JdbcTemplet jdbcTemplet = App.get("master");

        return jdbcTemplet.findList(sql, PersonLayer.class, personLayer.getMemberId(), personLayer.getDepth());

    }

    public List<PersonLayer> layerParentNoDepth(PersonLayer personLayer) {
        String sql = "WITH RECURSIVE r AS (\n" +
                "\n" +
                "    SELECT *,1 as depth FROM member_status WHERE member_id= ?\n" +
                "        union   ALL\n" +
                "        SELECT t.*,r.depth+1 as depth FROM member_status t, r WHERE t.member_id= r.parent_id\n" +
                "    )\n" +
                "\n" +
                "SELECT * FROM r ORDER BY depth";
        JdbcTemplet jdbcTemplet = App.get("master");

        return jdbcTemplet.findList(sql, PersonLayer.class, personLayer.getMemberId());

    }

    /**
     * 更新父亲节点的有效用户
     * @param memberId
     */
    public void updateValidMember(Long memberId){
        String sql="UPDATE member_status SET next_person_num=(SELECT count(distinct person_id) FROM member_order where person_id in(SELECT member_id FROM member_status where parent_id=?) and order_state=2) WHERE member_id=?";
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.execute(sql,memberId,memberId);

    }
}
