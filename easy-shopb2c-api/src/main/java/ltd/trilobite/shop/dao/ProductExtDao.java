package ltd.trilobite.shop.dao;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.shop.dao.entry.ProductExt;
import ltd.trilobite.shop.dao.entry.ProductReExt;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductExtDao extends BaseDao<ProductExt>{
    public ProductExt findById(Long memberProductId) {
        ProductExt productExt = new ProductExt();
        productExt.setMemberProductId(memberProductId);
        return findOne(productExt, ProductExt.class);
    }

    /**
     * 获取用于奖励的部分
     * @param memberProductId
     * @return
     */
    public Double getRewardPrice(Long memberProductId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select reward_price from product_ext where member_product_id=?", BigDecimal.class, memberProductId).doubleValue();
    }

    public List<Map<String, Object>> findList() {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select mp.*,pe.*,pr.in_price as re_in_price,pr.title as re_title, pr.title1 as re_title1,pr.price as re_price,pr.reward_price as re_reward_price,pr.self_profit as re_self_profit FROM\n" +
                "(\n" +
                "	select * \n" +
                "	from member_product\n" +
                "	where product_type_id=2\n" +
                ") mp\n" +
                "left join product_ext pe on pe.member_product_id=mp.member_product_id\n" +
                "left join product_re_ext pr on pr.member_product_id=mp.member_product_id";
        return jdbcTemplet.findList(sql);
    }

    public Result myGoodsList(RestForm form){
//        SELECT
//        pe.member_product_id, pe.title, pe.title1, pe.smail_image,mp.price,
//                pr.title as re_title,pr.title1 as re_title1,pr.price as re_price,
//        (select case when count(1)>0 then 1 else 0 end  from member_order where person_id=2 and member_product_id=pe.member_product_id) as is_re_consumption
//        FROM product_ext pe
//        left join member_product mp on mp.member_product_id=pe.member_product_id
//        left join product_re_ext pr on pr.member_product_id=pe.member_product_id
//        where mp.on_off=0
        JdbcTemplet jdbcTemplet = App.get("master");
        List<Object> params = new ArrayList<>();
        params.add(Long.parseLong(form.getHeader().get("personId")));
        SQL sql = new SQL();
        sql.select().cols("pe.member_product_id, pe.title, pe.title1, pe.smail_image,mp.price,pr.title as re_title, pr.title1 as re_title1, pr.price as re_price",
                "(select case when count(1)>0 then 2 else 1 end  from member_order where person_id=? and order_state in (2,3) and is_re_consumption=1 and member_product_id=pe.member_product_id) as is_re_consumption")
                .from().t("product_ext pe")
                .leftJoin().fix("member_product mp on mp.member_product_id=pe.member_product_id")
                .leftJoin().fix("product_re_ext pr on pr.member_product_id=pe.member_product_id")
                .where().fix("mp.on_off=0 and mp.product_type_id=2");
        SQL countsql = new SQL();
        countsql.select().cols("count(1)").from(sql.toString(), "a");
        sql.limit(form.get("start"), form.get("pageSize"));
        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, params.toArray(new Object[]{}));
    }

    public Map<String, Object> findDetail(Long personId, Long memberProductId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select pe.*,mp.price,mp.html_height,\n" +
                "(select case when count(1)>0 then 2 else 1 end from member_order where person_id=? and order_state in (2,3) and is_re_consumption=1 and member_product_id=pe.member_product_id) as is_re_consumption \n" +
                "from (\n" +
                "	SELECT member_product_id, title, title1, smail_image, introl_url\n" +
                "	FROM product_ext where member_product_id=?\n" +
                ") pe\n" +
                "left join member_product mp on mp.member_product_id=pe.member_product_id\n" +

                "where mp.on_off=0";
        String resourceSql = "select url,ext_type from product_res where member_product_id=?";
        Map<String, Object> map = jdbcTemplet.getMap(sql, personId, memberProductId);
//        if(Integer.parseInt(map.get("isReConsumption").toString()) == 2){
//            map.put("title", map.get("reTitle"));
//            map.put("title1", map.get("reTitle1"));
//            map.put("price", map.get("rePrice"));
//        }
//        map.remove("reTitle");
//        map.remove("reTitle1");
//        map.remove("rePrice");
        List<Map<String, Object>> resources = jdbcTemplet.findList(resourceSql, memberProductId);
        map.put("resources", resources);
        return map;
    }
}
