package ltd.trilobite.shop.dao;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.shop.dao.entry.RecommendRewardConfig;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RecommendRewardConfigDao extends BaseDao<RecommendRewardConfig>{
    public Map<Integer, Double> getConfigs() {
        JdbcTemplet jdbcTemplet = App.get("master");
        List<RecommendRewardConfig> list = jdbcTemplet.findList("select * from recommend_reward_config",
                RecommendRewardConfig.class);
        Map<Integer, Double> map = list.stream().collect(Collectors.toMap(RecommendRewardConfig::getLayer, RecommendRewardConfig::getProportion));
        return Collections.unmodifiableMap(map);
    }

    public Integer getMaxRebateLayer() {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select max(layer) from recommend_reward_config", Integer.class);

    }
}
