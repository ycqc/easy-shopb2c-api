package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "street")
public class Street {

  private String code;
  private String name;
  private String areacode;
  private String provincecode;
  private String citycode;


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getAreacode() {
    return areacode;
  }

  public void setAreacode(String areacode) {
    this.areacode = areacode;
  }


  public String getProvincecode() {
    return provincecode;
  }

  public void setProvincecode(String provincecode) {
    this.provincecode = provincecode;
  }


  public String getCitycode() {
    return citycode;
  }

  public void setCitycode(String citycode) {
    this.citycode = citycode;
  }

}
