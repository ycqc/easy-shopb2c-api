package ltd.trilobite.shop.dao.entry;


import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name="logistics_invoice")
public class LogisticsInvoice {
  @Id(type = IdType.Seq)
  private Long logisticsInvoiceId;
  private Long orderId;
  private Long logisticsCompanyId;
  private Double price;
  private String num;


  public Long getLogisticsInvoiceId() {
    return logisticsInvoiceId;
  }

  public void setLogisticsInvoiceId(Long logisticsInvoiceId) {
    this.logisticsInvoiceId = logisticsInvoiceId;
  }


  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }


  public Long getLogisticsCompanyId() {
    return logisticsCompanyId;
  }

  public void setLogisticsCompanyId(Long logisticsCompanyId) {
    this.logisticsCompanyId = logisticsCompanyId;
  }


  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }


  public String getNum() {
    return num;
  }

  public void setNum(String num) {
    this.num = num;
  }

}
