package ltd.trilobite.shop.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "base_data")
public class BaseData {
    @Id(type = IdType.Seq)
    private Long baseId;
    private String code;
    private String label;
    private Integer typeId;


    public Long getBaseId() {
        return baseId;
    }

    public void setBaseId(Long baseId) {
        this.baseId = baseId;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

}
