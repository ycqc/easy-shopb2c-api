package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

import java.util.Date;

@Table(name = "person")
public class Person implements java.io.Serializable {
  @Id(type = IdType.Seq)
  private Long personId;
  private String name;
  private String phoneNum;
  private String pass;
  private Integer treamNum;
  private Integer treamPark;
  private Integer memberType;
  private Date createTime;
  private Integer gender;
  private Integer lockState;
  private String payPass;
  private String headImageUrl;


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getPhoneNum() {
    return phoneNum;
  }

  public void setPhoneNum(String phoneNum) {
    this.phoneNum = phoneNum;
  }


  public String getPass() {
    return pass;
  }

  public void setPass(String pass) {
    this.pass = pass;
  }


  public Integer getTreamNum() {
    return treamNum;
  }

  public void setTreamNum(Integer treamNum) {
    this.treamNum = treamNum;
  }


  public Integer getTreamPark() {
    return treamPark;
  }

  public void setTreamPark(Integer treamPark) {
    this.treamPark = treamPark;
  }


  public Integer getMemberType() {
    return memberType;
  }

  public void setMemberType(Integer memberType) {
    this.memberType = memberType;
  }

  public Integer getGender() {
    return gender;
  }

  public void setGender(Integer gender) {
    this.gender = gender;
  }


  public Integer getLockState() {
    return lockState;
  }

  public void setLockState(Integer lockState) {
    this.lockState = lockState;
  }


  public String getPayPass() {
    return payPass;
  }

  public void setPayPass(String payPass) {
    this.payPass = payPass;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getHeadImageUrl() {
    return headImageUrl;
  }

  public void setHeadImageUrl(String headImageUrl) {
    this.headImageUrl = headImageUrl;
  }
}
