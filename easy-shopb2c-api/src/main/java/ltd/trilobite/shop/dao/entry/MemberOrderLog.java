package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

import java.util.Date;

@Table(name = "member_order_log")
public class MemberOrderLog {
  @Id(type = IdType.Seq)
  private Integer memberOrderLogId;
  private Integer status;
  private Date createTime;
  private Long createPerson;
  private Long orderId;


  public Integer getMemberOrderLogId() {
    return memberOrderLogId;
  }

  public void setMemberOrderLogId(Integer memberOrderLogId) {
    this.memberOrderLogId = memberOrderLogId;
  }


  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Long getCreatePerson() {
    return createPerson;
  }

  public void setCreatePerson(Long createPerson) {
    this.createPerson = createPerson;
  }


  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

}
