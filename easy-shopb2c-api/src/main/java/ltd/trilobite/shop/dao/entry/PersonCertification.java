package ltd.trilobite.shop.dao.entry;


import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

import java.util.Date;

@Table(name = "person_certification")
public class PersonCertification {

  @Id(type = IdType.None)
  private Long personId;
  private String realName;
  private String cardNo;
  private String cardBg;
  private String cardFront;
  private String handImg;
  private Integer isAudit;
  private String auditMessage;
  private String province;
  private String city;
  private String area;
  private String street;
  private Integer auditState;
  private Date createTime;
  private Date updateTime;
  private Long auditPerson;


  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }


  public String getCardNo() {
    return cardNo;
  }

  public void setCardNo(String cardNo) {
    this.cardNo = cardNo;
  }


  public String getCardBg() {
    return cardBg;
  }

  public void setCardBg(String cardBg) {
    this.cardBg = cardBg;
  }


  public String getCardFront() {
    return cardFront;
  }

  public void setCardFront(String cardFront) {
    this.cardFront = cardFront;
  }


  public String getHandImg() {
    return handImg;
  }

  public void setHandImg(String handImg) {
    this.handImg = handImg;
  }


  public Integer getIsAudit() {
    return isAudit;
  }

  public void setIsAudit(Integer isAudit) {
    this.isAudit = isAudit;
  }


  public String getAuditMessage() {
    return auditMessage;
  }

  public void setAuditMessage(String auditMessage) {
    this.auditMessage = auditMessage;
  }


  public long getPersonId() {
    return personId;
  }

  public void setPersonId(long personId) {
    this.personId = personId;
  }


  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }


  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }


  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }


  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }


  public Integer getAuditState() {
    return auditState;
  }

  public void setAuditState(Integer auditState) {
    this.auditState = auditState;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }


  public Long getAuditPerson() {
    return auditPerson;
  }

  public void setAuditPerson(Long auditPerson) {
    this.auditPerson = auditPerson;
  }

}
