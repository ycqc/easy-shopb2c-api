package ltd.trilobite.shop.dao;

import ltd.trilobite.shop.dao.entry.PersonCertification;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.jdbc.SqlParm;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 实名认证
 */
public class PersonCertificationDao extends BaseDao<PersonCertification> {

    /**
     * 根据用户ID查询实名认证信息
     * @param personId 用户ID
     * @return
     */
    public Map<String, Object> findByPersonId(Long personId) {
//        SQL:
//        select real_name,card_no,card_bg,card_front,is_audit,audit_message,person_id,province,city,area,street,audit_state,
//                to_char(create_time,'YYYY-MM-DD HH24:MI:SS') create_time,
//                to_char(update_time,'YYYY-MM-DD HH24:MI:SS') as update_time
//        from person_certification
//        where person_id=?

        JdbcTemplet jdbcTemplet = App.get("master");
        SQL sql = new SQL();
        sql.select().cols("real_name,card_no,card_bg,card_front,hand_img,is_audit,audit_message,province,city,area,street,audit_state",
                "to_char(create_time,'YYYY-MM-DD HH24:MI:SS') create_time",
                "to_char(update_time,'YYYY-MM-DD HH24:MI:SS') as update_time")
                .from().t("person_certification")
                .where().fix("person_id=?");
        return jdbcTemplet.getMap(sql.toString(), personId);
    }

    /**
     * 根据用户ID查询审核状态
     * @param personId
     * @return
     */
    public PersonCertification findById(Long personId) {
        PersonCertification personCertification = new PersonCertification();
        personCertification.setPersonId(personId);
        return findOne(personCertification, PersonCertification.class);
    }

    /**
     * 根据用户ID查询审核状态
     * @param personId
     * @return
     */
    public Integer findAuditStateByPersonId(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select audit_state from person_certification where person_id=?", Integer.class, personId);
    }

    /**
     * 分页查询实名认证信息
     * @param form
     * @return
     */
    public Result findNaviList(RestForm form) {
//        SQL:
//        select  p.name,p.phone_num,p2.name as audit_name,pv.name as province_name, cy.name as city_name,a.name as area_name, s.name as street_name, c.*
//        from
//        (
//                select
//                real_name,card_no,card_bg,card_front,hand_img,is_audit,audit_message,person_id,province,city,area,street,audit_state,audit_person,
//                to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time,
//                to_char(update_time, 'YYYY-MM-DD HH24:MI:SS') as update_time
//                from person_certification
//                where card_no='' and real_name like '' and is_audit=1 and audit_state=1
//        ) c
//        left join person p on p.person_id=c.person_id
//        left join person p2 on p2.person_id=c.audit_person
//        left join province pv on pv.code=c.province
//        left join city cy on cy.code=c.city
//        left join area a on a.code=c.area
//        left join street s on s.code=c.street
//        where p.name like '' and p.phone_num=''
//        order by c.create_time desc
        JdbcTemplet jdbcTemplet = App.get("master");

        List<Object> param = new ArrayList<>();

        SQL sql = new SQL();
        SQL subsql = new SQL();
        SQL countsql = new SQL();

        subsql.select().cols("real_name,card_no,card_bg,card_front,hand_img,is_audit,audit_message,person_id,province,city,area,street,audit_state,audit_person",
                "to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time",
                "to_char(update_time,'YYYY-MM-DD HH24:MI:SS') as update_time")
                .from().t("person_certification")
                .where(true);

        //身份证号
        String cardNo = form.get("cardNo");
        if (StringUtils.isNotBlank(cardNo)) {
            subsql.fix(" and card_no=?");
            param.add(cardNo.trim());
        }

        //姓名
        String realName = form.get("realName");
        if (StringUtils.isNotBlank(realName)) {
            subsql.fix(" and real_name like ?");
            param.add(realName.trim() + "%");
        }

        //是否审核通过 1-通过 2-未通过
        String isAudit = form.get("isAudit");
        if (StringUtils.isNotBlank(isAudit)) {
            subsql.fix(" and is_audit=?");
            param.add(Integer.parseInt(isAudit));
        }

        //审核状态 1-待审核 2-审核中 3-已审核
        String auditState = form.get("auditState");
        if (StringUtils.isNotBlank(auditState)) {
            subsql.fix(" and audit_state=?");
            param.add(Integer.parseInt(auditState));
        }

        sql.select().cols("p.name,p.phone_num,p2.name as audit_name,pv.name as province_name, cy.name as city_name,a.name as area_name, s.name as street_name, c.*")
                .from().subsql(subsql, "c")
                .leftJoin().t("person p").on(new SqlParm("p.person_id").eq().v("c.person_id"))
                .leftJoin().t("person p2").on(new SqlParm("p2.person_id").eq().v("c.audit_person"))
                .leftJoin().t("province pv").on(new SqlParm("pv.code").eq().v("c.province"))
                .leftJoin().t("city cy").on(new SqlParm("cy.code").eq().v("c.city"))
                .leftJoin().t("area a").on(new SqlParm("a.code").eq().v("c.area"))
                .leftJoin().t("street s").on(new SqlParm("s.code").eq().v("c.street"))
                .where(true);

        //用户名
        String name = form.get("name");
        if (StringUtils.isNotBlank(name)) {
            sql.fix(" and p.name like ?");
            param.add(name.trim() + "%");
        }

        //手机号
        String phoneNum = form.get("phoneNum");
        if (StringUtils.isNotBlank(phoneNum)) {
            sql.fix(" and p.phone_num=?");
            param.add(phoneNum.trim());
        }

        countsql.select().cols("count(1)").from(sql.toString(), "t");

        sql.order("c.create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, param.toArray());

    }
}
