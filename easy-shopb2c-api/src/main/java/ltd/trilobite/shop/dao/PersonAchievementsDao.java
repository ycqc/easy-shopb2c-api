package ltd.trilobite.shop.dao;

import ltd.trilobite.shop.dao.entry.PersonAchievements;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

import java.util.*;

public class PersonAchievementsDao extends BaseDao<PersonAchievements> {
    public void delForSrcId(Long orderId) {
        String sql = "delete from person_achievements where order_id=?";
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.execute(sql, orderId);
    }

    public List<PersonAchievements> findNewAdd(Date startTime, Date endTime) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select * from person_achievements where create_time>=? and create_time<=?";

        return jdbcTemplet.findList(sql, PersonAchievements.class, startTime, endTime);
    }

    public Map<Long, Long> findTem(Date endTime, String persons) {
        Map<Long, Long> map = new HashMap<>();
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select sum(num) as team_num,person_id from person_achievements where create_time<=? and person_id in(" + persons + ") group by person_id";
        jdbcTemplet.findList(sql, (e) -> {
            map.put(Long.parseLong(e.get("personId").toString()), Long.parseLong(e.get("teamNum").toString()));
        }, endTime);
        return map;
    }


    public Map<String, Object> myAchievements(Long personId) {
        String sql = "select count(1) from person_achievements where order_person_id in(select member_id from member_status where parent_id=" + personId + ")";
        String sql1 = "select count(1) from person_achievements where person_id=" + personId;
        String sql2 = "select count(distinct order_person_id) from person_achievements where order_person_id in(select member_id from member_status where parent_id="+personId+")";
        JdbcTemplet jdbcTemplet = App.get("master");
        Map<String, Object> map = new HashMap<>();
        map.put("personCount", jdbcTemplet.getObject(sql2, Long.class));
        map.put("line", jdbcTemplet.getObject(sql, Long.class));
        map.put("all", jdbcTemplet.getObject(sql1, Long.class));
        return map;
    }

    public Result myDayList(Long personId, RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");

        SQL countsql = new SQL();

        SQL sql = new SQL();
        //select count(1),to_char(create_time,'YYYY-MM-dd') as create_time from person_achievements where person_id=1
        sql.select().cols("count(1) as count", "to_char(create_time,'YYYY-MM-dd') as create_time")
                .from().t("person_achievements").where().add(true, "person_id", personId.toString())
                .fix(" group by to_char(create_time,'YYYY-MM-dd') ");

        List<Object> param = new ArrayList<Object>();


        countsql.select().cols("count(1)").from(sql.toString(), "a");

        sql.order("create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, param.toArray());

    }

}
