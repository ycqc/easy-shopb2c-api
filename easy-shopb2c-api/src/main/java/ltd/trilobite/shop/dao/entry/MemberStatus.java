package ltd.trilobite.shop.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "member_status")
public class MemberStatus {
    @Id(type = IdType.Seq)
    private Long memberStatusId;
    private Long memberId;
    private Long parentId;


    public Long getMemberStatusId() {
        return memberStatusId;
    }

    public void setMemberStatusId(Long memberStatusId) {
        this.memberStatusId = memberStatusId;
    }


    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }


    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

}
