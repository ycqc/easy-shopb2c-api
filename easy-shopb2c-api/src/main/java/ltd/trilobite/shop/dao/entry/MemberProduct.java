package ltd.trilobite.shop.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "member_product")
public class MemberProduct {
    @Id
    private Long memberProductId;
    private String name;
    private Double price;
    private Integer onOff;
    private Long productTypeId;
    private Integer htmlHeight;

    public Long getMemberProductId() {
        return memberProductId;
    }

    public void setMemberProductId(Long memberProductId) {
        this.memberProductId = memberProductId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }


    public Integer getOnOff() {
        return onOff;
    }

    public void setOnOff(Integer onOff) {
        this.onOff = onOff;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Integer getHtmlHeight() {
        return htmlHeight;
    }

    public void setHtmlHeight(Integer htmlHeight) {
        this.htmlHeight = htmlHeight;
    }
}
