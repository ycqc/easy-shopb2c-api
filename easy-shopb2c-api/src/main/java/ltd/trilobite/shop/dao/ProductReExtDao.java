package ltd.trilobite.shop.dao;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.shop.dao.entry.MemberProduct;
import ltd.trilobite.shop.dao.entry.ProductReExt;

import java.math.BigDecimal;

public class ProductReExtDao extends BaseDao<ProductReExt>{

    /**
     * 获取用于奖励的部分
     * @param memberProductId
     * @return
     */
    public Double getRewardPrice(Long memberProductId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select reward_price from product_re_ext where member_product_id=?", BigDecimal.class, memberProductId).doubleValue();
    }

    /**
     * 获取产品价格
     * @param memberProductId
     * @return
     */
    public Double getPrice(Long memberProductId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select price from product_re_ext where member_product_id=?", BigDecimal.class, memberProductId).doubleValue();
    }

    public ProductReExt findById(Long memberProductId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        ProductReExt productReExt = new ProductReExt();
        productReExt.setMemberProductId(memberProductId);
        return jdbcTemplet.one(productReExt, ProductReExt.class);
    }

    public void save(ProductReExt productReExt) {
        JdbcTemplet jdbcTemplet = App.get("master");
        long count = jdbcTemplet.getObject("select count(1) from product_re_ext where member_product_id=?", Long.class, productReExt.getMemberProductId());
        if(count == 0){
            add(productReExt);
        }else{
            update(productReExt);
        }
    }
}
