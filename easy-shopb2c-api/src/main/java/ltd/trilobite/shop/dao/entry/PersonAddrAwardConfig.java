package ltd.trilobite.shop.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "person_addr_award_config")
public class PersonAddrAwardConfig {
  @Id
  private Long personAddrAwardConfigId;
  private String addrCode;
  private String addrName;
  private Long personId;


  public Long getPersonAddrAwardConfigId() {
    return personAddrAwardConfigId;
  }

  public void setPersonAddrAwardConfigId(Long personAddrAwardConfigId) {
    this.personAddrAwardConfigId = personAddrAwardConfigId;
  }


  public String getAddrCode() {
    return addrCode;
  }

  public void setAddrCode(String addrCode) {
    this.addrCode = addrCode;
  }


  public String getAddrName() {
    return addrName;
  }

  public void setAddrName(String addrName) {
    this.addrName = addrName;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }

}
