package ltd.trilobite.shop.dao;

import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

import java.util.List;

public class BaseDao<T> {
    /**
     * 新增
     * @param t
     */
    public void add(T t){
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.save(t);
    }


    /**
     * 新增
     * @param obj
     */
    public void adds(List<Object> obj){
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.adds(obj);
    }

    public Long getSeq(String table){
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select nextval(\'"+table+"_seq\')",Long.class);
    }

    public <E> E findFunc(T t,Class<E> tClass,String func){
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.findFunction(t,tClass,func);
    }

    /**
     * 修改
     * @param t
     */
    public void update(T t){
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.update(t);
    }

    /**
     * 删除
     * @param t
     */
    public void del(T t){
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.delete(t);
    }

    /**
     * 删除
     * @param t
     */
    public T findOne(T t,Class<T> tClass){
        JdbcTemplet jdbcTemplet = App.get("master");

        return jdbcTemplet.one(t,tClass);
    }

    /**
     * 查询单表
     * @param t
     * @param tClass
     * @return
     */
    public List<T> list(T t,Class<T> tClass){
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.findTable(t,tClass);
    }
}