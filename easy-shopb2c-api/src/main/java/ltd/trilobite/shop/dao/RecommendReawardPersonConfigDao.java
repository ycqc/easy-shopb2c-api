package ltd.trilobite.shop.dao;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.shop.dao.entry.RecommendReawardPersonConfig;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RecommendReawardPersonConfigDao extends BaseDao<RecommendReawardPersonConfig> {
    public Map<Integer, Integer> getConfigs() {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select * from recommend_reaward_person_config";
        List<RecommendReawardPersonConfig> list = jdbcTemplet.findList(sql, RecommendReawardPersonConfig.class);
        Map<Integer, Integer> map = list.stream().collect(Collectors.toMap(RecommendReawardPersonConfig::getPersonNum, RecommendReawardPersonConfig::getLayer));
        return Collections.unmodifiableMap(map);
    }

    /**
     * 获取最大的推荐人数
     * @return
     */
    public Integer getMaxPersonNum() {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select max(person_num) from recommend_reaward_person_config", Integer.class);
    }

    public Integer getMaxLayer() {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject("select max(layer) from recommend_reaward_person_config", Integer.class);
    }
}